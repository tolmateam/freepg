#include "freepg-server.h"
#include "freepg-common/freepg-common.h"
#include "freepg-common/networking.h"
#include "freepg-common/types.h"
#include <wchar.h>
#include <string.h>
#include <assert.h>


static ServerHandle handle;

static bool _handleBeforeGameStage(void);
static bool _handleGameInProgressStages(void);
static bool _handleGameOverStage(void);

int main(int argc, char **argv)
{
    freepgCommonInit();
    startServer(&handle);

    bool correctProcessing = true;
    do
    {
        switch(handle.gameStage)
        {
        case BEFORE_GAME_STAGE:
            correctProcessing = _handleBeforeGameStage();
            break;
        case GAME_FIRST_STAGE:
        case GAME_SECOND_STAGE:
        case GAME_THIRD_STAGE:
            correctProcessing = _handleGameInProgressStages();
            break;
        case GAME_OVER_STAGE:
            correctProcessing = _handleGameOverStage();
            break;
        default:
            assert(!"Unknown game state");
            break;
        }

        if(correctProcessing)
        {
            ErrorCauses res = pollingClients(&handle);
            correctProcessing = (res == OK_CAUSE || res == CONNECTION_CAUSE);
            if(res != OK_CAUSE)
                logPrintf("Polling client error %d\n", (int)res);
        }

    } while(correctProcessing);

    stopServer(&handle);
    freepgCommonRelease();

    return correctProcessing ? 0 : 1;
}

bool _handleBeforeGameStage(void)
{
    // Take into account who has not respond
    logPrintf("There are %zu players in the game\n", handle.playersAmount);
    for(size_t i = 0; i < handle.playersAmount; i++)
    {
        PlayerData *ph = &handle.players[i];
        logPrintf("    - player %zu: name='%ls', ready=%s, respond=%s, network-error-before=%d\n",
                 i, ph->nickname, ph->ready ? "true" : "false", ph->respond ? "true" : "false", ph->networkErrorCounter);
        if(!ph->respond && ++ph->networkErrorCounter > MAX_PROTOCOL_ERRORS_ALLOWED)
        {   // He or she have annoyed us violating the game protocol, just knock them out the game!

            // Firstly, add disconnect information to appropriate data collection if we can
            if(handle.playersToDisconnectAmount < MAX_PLAYERS_TO_DISCONNECT)
            {
                DisconnectedPlayer *dp = &handle.playersToDisconnect[handle.playersToDisconnectAmount++];
                dp->cause = MOVE_TIMEOUT;
                wcsncpy(dp->nickname, ph->nickname, getWideStringLength(ph->nickname) + 1);
            }

            // Finally, remove the buddy from the handle
            if(i+1 < handle.playersAmount)
                memcpy(ph, &handle.players[i+1], (handle.playersAmount - (i+1)) * sizeof(PlayerData));
            handle.playersAmount--;
            i--;
            logPrintf("Knock the buddy out the game\n");
        }
    }
    logPrintf("\n\n");

    if(handle.gameStage == BEFORE_GAME_STAGE && handle.playersAmount >= MIN_PLAYERS)
    {
        // Whether we have enough players to start and all of them are ready
        bool allPlayersReady = true;
        for(size_t i = 0; i < handle.playersAmount; i++)
            if(!handle.players[i].ready)
            {
                allPlayersReady = false;
                break;
            }
        // Yes. Start the game!
        if(allPlayersReady)
            handle.gameStage = GAME_FIRST_STAGE;
    }

    return true;
}

bool _handleGameInProgressStages(void)
{
    logPrintf("The game is in progress!\n");
    return true;
}

bool _handleGameOverStage(void)
{
    return true;
}
