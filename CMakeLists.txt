cmake_minimum_required(VERSION 3.1)
project(freepg)

add_subdirectory(freepg-common)
#add_subdirectory(freepg-server)
add_subdirectory(freepg-client)
