#pragma once

#include "resource.h"
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QApplication>

// PG- POWERGRID
// PB - PLAYERBUTTON
// RES - RESOURCES
enum {
  WINDOW_WIDTH = 1600,
  WINDOW_HEIGHT = 900,

  RES_LABEL_START_X = 900,
  RES_LABEL_START_Y = 80,
  RES_LABEL_GAP = 10,
  RES_LABEL_WIDTH = 80,
  RES_LABEL_HEIGHT = 30,

  PB_START_X = 100,
  PB_START_Y = 10,
  PB_GAP = 80,
  PB_WIDTH = 80,
  PB_HEIGHT = 30,

  PG_LABEL_START_X = 100,
  PG_LABEL_START_Y = 50,
  PG_LABEL_GAP = 100,
  PG_WIDTH = 80,
  PG_HEIGHT = 80
};

class QPushButton;
class QLabel;
class MainWindow : public QWidget
{
Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    void drawResources(Resource stock);
    void drawPlayersPowerGrids(int id, std::vector<int> powerGrids);
    void drawPlayersButtons(int id);
    void drawAuction(std::vector<int> powerGrids);

signals:
//    void counterReached();

private slots:
//      void showPlayer(bool checked);
//    void slotButtonClicked(bool checked);

private:

    QPushButton *player_button[6];
    QLabel *player_res_label[5];
    QLabel *player_pg[6][3];
    QPushButton *actn_b[4];
    QLabel *actn_l[4];
};
