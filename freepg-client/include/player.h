#pragma once

#include "resource.h"
#include "mainwindow.h"
#include <QString>
#include <vector>

class Player
{
public:
    Player() { }

    int id = -1;
    QString name;
    std::vector<int> powerGrids;
    Resource stock = Resource(0, 0, 0, 0, 0);

private:

};


class Players
{
public:
    enum Consts {
        PLAYERS_COUNT = 5
    };

public:
    static Players& instance();

private:
    Players() { }
    ~Players() { }
    Players(const Players &) = delete;
    Players& operator=(const Players &) = delete;

public:
    Player *addPlayer(int id);
    Player *getById(int id);
    const Player *getById(int id) const;

private:
    static Players* _instance;
    Player _players[PLAYERS_COUNT];
};
