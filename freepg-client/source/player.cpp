#include "player.h"

Players* Players::_instance = nullptr;

Players& Players::instance()
{
    if (!Players::_instance) {
        Players::_instance = new Players;
    }
    return *Players::_instance;
}


Player* Players::addPlayer(int id)
{
    for (auto& p : _players) {
        if (p.id < 0) {
            p.id = id;
            return &p;
        }
    }
    return nullptr;
}

Player* Players::getById(int id)
{
    for (auto& p : _players) {
        if (p.id == id)
            return &p;
    }
    return nullptr;
}

const Player* Players::getById(int id) const
{
    for (auto& p : _players) {
        if (p.id == id)
            return &p;
    }
    return nullptr;
}
