#include "player.h"
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
    // Set size of the window
    setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    for (int i = 0; i < 6; i++)
        for (int j = 0; j < 3; j++)
            player_pg[i][j] = nullptr;

    connect(player_button[1], SIGNAL(clicked(bool)), this, SLOT(showPlayer(bool)));
//    Connect button to be clicked
//    connect(m_button, SIGNAL(clicked(bool)), this, SLOT(slotButtonClicked(bool)));
//    connect(this, SIGNAL(counterReached()), QApplication::instance(), SLOT(quit()));
}

//void Window::slotButtonClicked(bool checked)
//{
//    if (checked) {
//        m_button->setText("Checked");
//    } else {
//        m_button->setText("MyButton");
//    }

//    m_cnt++;
//    if (m_cnt == 10)
//        emit counterReached();
//}

//void MainWindow::showPlayer(bool checked)
//{
//    Player *player = Players::instance().getById(2);
//    if (player == nullptr) {
//        // TODO
//    }
//    if (checked) {
//        this->drawResources();
//        this->drawPlayersPowerGrids();
//    }
//}


void MainWindow::drawResources(Resource stock)
{
    player_res_label[0] = new QLabel(QString("Money= %1").arg(stock.money), this);
    player_res_label[1] = new QLabel(QString("Coal= %1").arg(stock.coal), this);
    player_res_label[2] = new QLabel(QString("Masut= %1").arg(stock.masut), this);
    player_res_label[3] = new QLabel(QString("Scrap= %1").arg(stock.scrap), this);
    player_res_label[4] = new QLabel(QString("Uran= %1").arg(stock.uran), this);

    for (int i = 0; i < 5; i++) {
        player_res_label[i]->setGeometry(RES_LABEL_START_X + i * RES_LABEL_START_Y, RES_LABEL_GAP, RES_LABEL_WIDTH, RES_LABEL_HEIGHT);
    }
}

void MainWindow::drawPlayersPowerGrids(int id, std::vector<int> powerGrids)
{
    for (int i = 0; i < powerGrids.size(); i++) {
        if (player_pg[id][i] == nullptr) {
            player_pg[id][i] = new QLabel(QString("PowerGrid\n %1").arg(powerGrids[i]), this);
            player_pg[id][i]->setGeometry(PG_LABEL_START_X + i*PG_LABEL_GAP, PG_LABEL_START_Y, PG_WIDTH, PG_HEIGHT);
        }
    }
}

void MainWindow::drawPlayersButtons(int id)
{
    int cnt = 0;
    for (int i = 0; i < 6; i++) {
        if (i != id) {
            player_button[i] = new QPushButton(QString("Player %1").arg(i), this);
            player_button[i]->setGeometry(PB_START_X + cnt * PB_GAP, PB_START_Y, PB_WIDTH, PB_HEIGHT);
            player_button[i]->setCheckable(true);
            cnt++;
        }
    }
}

void MainWindow::drawAuction(std::vector<int> powerGrids)
{
    int canChoose = 4;
    for (int i = 0; i < canChoose; i++) {
        actn_b[i] = new QPushButton(QString("PowerGrid\n %1").arg(powerGrids[i]), this);
        actn_b[i]->setGeometry(600+ i*80, 200, PG_WIDTH, PG_HEIGHT);
        actn_b[i]->setCheckable(true);
    }

    for (int i = 0; i < 4; i++) {
        actn_l[i] = new QLabel(QString("PowerGrid\n %1").arg(powerGrids[canChoose + i]), this);
        actn_l[i]->setGeometry(600+ i*80, 200 + 80, PG_WIDTH, PG_HEIGHT);
    }
}
