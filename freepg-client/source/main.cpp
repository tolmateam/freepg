#include "main.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Player client;
    client.id = 0;
    client.stock.money = 0;
    client.stock.coal = 0;
    client.stock.masut = 0;
    client.stock.scrap = 0;
    client.stock.uran = 0;

    Player players[5];

    MainWindow window;

    client.powerGrids.push_back(1);
    client.powerGrids.push_back(2);
    client.powerGrids.push_back(3);


    window.drawResources(client.stock);

    window.drawPlayersButtons(client.id);

    window.drawPlayersPowerGrids(client.id, client.powerGrids);

    std::vector<int> pg_auction;
    pg_auction = {1, 2, 3, 4, 5, 6 ,7 ,8};
    window.drawAuction(pg_auction);

    window.show();
    return a.exec();
}
