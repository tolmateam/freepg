#include <freepg-common/types.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>


//static const City cities[] =
//{
//    // RED_ZONE
//    { "Kansas City",     1, RED_ZONE },
//    { "Oklahoma City",   2, RED_ZONE },
//    { "Dallas",          3, RED_ZONE },
//    { "Houstom",         4, RED_ZONE },
//    { "Memphis",         5, RED_ZONE },
//    { "New Orleans",     6, RED_ZONE },
//    { "Birmingham",      7, RED_ZONE },

//    // GREEN_ZONE
//    { "Atlanta",         8, GREEN_ZONE },
//    { "Tampa",           9, GREEN_ZONE },
//    { "Savannah",       10, GREEN_ZONE },
//    { "Jacksonville",   11, GREEN_ZONE },
//    { "Miami",          12, GREEN_ZONE },
//    { "Raleigh",        13, GREEN_ZONE },
//    { "Norfolk",        14, GREEN_ZONE },

//    // BLUE_ZONE
//    { "San Francisco",  15, BLUE_ZONE },
//    { "Los Angeles",    16, BLUE_ZONE },
//    { "San Diego",      17, BLUE_ZONE },
//    { "Las Vegas",      18, BLUE_ZONE },
//    { "Salt Lake City", 19, BLUE_ZONE },
//    { "Phoenix",        20, BLUE_ZONE },
//    { "Santa Fe",       21, BLUE_ZONE },

//    // BLUE_ZONE
//    { "Detroit",        22, BLUE_ZONE },
//    { "Buffalo",        23, BLUE_ZONE },
//    { "Pitsburgh",      24, BLUE_ZONE },
//    { "Wahingtong",     25, BLUE_ZONE },
//    { "Philadelphia",   26, BLUE_ZONE },
//    { "New York",       27, BLUE_ZONE },
//    { "Boston",         28, BLUE_ZONE },

//    // YELLOW ZONE
//    { "Fargo",          29, YELLOW_ZONE },
//    { "Duluh",          30, YELLOW_ZONE },
//    { "Minneapolis",    31, YELLOW_ZONE },
//    { "Chicago",        32, YELLOW_ZONE },
//    { "St.Louis",       33, YELLOW_ZONE },
//    { "Cincinati",      34, YELLOW_ZONE },
//    { "Knoxville",      35, YELLOW_ZONE },

//    // MAGENTA_ZONE
//    { "Seattle",        36, MAGENTA_ZONE },
//    { "PortLand",       37, MAGENTA_ZONE },
//    { "Boise",          38, MAGENTA_ZONE },
//    { "Billings",       39, MAGENTA_ZONE },
//    { "Cheyenne",       40, MAGENTA_ZONE },
//    { "Denver",         41, MAGENTA_ZONE },
//    { "Omaha",          42, MAGENTA_ZONE },
//};

//static const PowerLine powerLines[] =
//{
////Red Zone
//    { 1, 12, 5 },
//    { 1,  8, 2 },
//    { 2, 14, 5 },
//    { 2,  3, 3 },
//    { 3, 12, 5 },
//    { 3,  5, 4 },
//    { 7,  6, 5 },
//    { 7, 11, 6 },
//    { 6,  7, 5 },
//    { 6, 12, 3 },
//    { 4,  8, 6 },
////Blue Zone
//    {15, 19, 27},
//    {15, 18, 14},
//    {15, 16, 9},
//    {19, 18, 18},
//    {16, 18, 9},
//    {16, 17, 3},
//    {18, 17, 9},
//    {18, 20, 15},
//    {17, 20, 14},
//    {19, 21, 28},
//    {18, 21, 27},
//    {20,21, 18},
//// Magenta Zone
//    {36, 39, 9},
//    {36, 37, 3},
//    {36, 38, 12},
//    {37, 38, 13},
//    {38, 39, 12},
//    {38, 40, 24},
//    {39, 40, 9},
//    {40, 41, 0},
//    {40, 42 , 14},
////Green Zone
//    {14, 13, 3},
//    {13, 8, 7},
//    {13, 10 , 7},
//    {8, 10, 7},
//    {10, 11 , 0},
//    {11, 9 , 4},
//    {9, 12, 4},
//// Yellow Zone
//    {29, 30, 6},
//    {29, 31, 6},
//    {30, 31, 5},
//    {32, 31, 8},
//    {32, 30, 12},
//    {32, 34, 7},
//    {34, 35, 6},
//    {33, 32, 10},
//    {33, 34, 12},
//// Brown Zone
//    {22, 23, 7},
//    {22, 24, 6},
//    {23, 24, 7},
//    {23, 27, 8},
//    {24, 25, 6},
//    {25, 26, 3},
//    {27, 26, 0},
//    {27, 28, 3},
//};

//static const PowerStation powerStations[] =
//{
//    //       { C  O  W  U } - { Coal, heating Oil, Waste, Uranium }
//    {  3, 1, { 0, 2, 0, 0 } }, // FT_HEATING_OIL: 2
//    {  4, 1, { 2, 0, 0, 0 } }, // FT_COAL: 2
//    {  5, 1, { 2, 2, 0, 0 } }, // FT_HYBRID: 2
//    {  6, 1, { 0, 0, 1, 0 } }, // FT_WASTE: 1
//    {  7, 2, { 0, 3, 0, 0 } }, // FT_HEATING_OIL: 3
//    {  8, 2, { 3, 0, 0, 0 } }, // FT_COAL: 3
//    {  9, 1, { 0, 1, 0, 0 } }, // FT_HEATING_OIL: 1
//    { 10, 2, { 2, 0, 0, 0 } }, // FT_COAL: 2
//    { 11, 2, { 0, 0, 0, 1 } }, // FT_URANIUM: 1
//    { 12, 2, { 2, 2, 0, 0 } }, // FT_HYBRID: 2
//    { 13, 1, { 0, 0, 0, 0 } }, // FT_NONE
//    { 14, 2, { 0, 0, 2, 0 } }, // FT_WASTE: 2
//    { 15, 3, { 2, 0, 0, 0 } }, // FT_COAL: 2
//    { 16, 3, { 0, 2, 0, 0 } }, // FT_HEATING_OIL: 2
//    { 17, 2, { 0, 0, 0, 1 } }, // FT_URANIUM: 1
//    { 18, 2, { 0, 0, 0, 0 } }, // FT_NONE
//    { 19, 3, { 0, 0, 2, 0 } }, // FT_WASTE: 2
//    { 20, 5, { 3, 0, 0, 0 } }, // FT_COAL: 3
//    { 21, 4, { 2, 2, 0, 0 } }, // FT_HYBRID: 2
//    { 22, 2, { 0, 0, 0, 0 } }, // FT_NONE
//    { 23, 3, { 0, 0, 0, 1 } }, // FT_URANIUM: 1
//    { 24, 4, { 0, 0, 2, 0 } }, // FT_WASTE: 2
//    { 25, 5, { 2, 0, 0, 0 } }, // FT_COAL: 2
//    { 26, 5, { 0, 2, 0, 0 } }, // FT_HEATING_OIL: 2
//    { 27, 3, { 0, 0, 0, 0 } }, // FT_NONE
//    { 28, 4, { 0, 0, 0, 1 } }, // FT_URANIUM: 1
//    { 29, 4, { 1, 1, 0, 0 } }, // FT_HYBRID: 1
//    { 30, 6, { 0, 0, 3, 0 } }, // FT_WASTE: 3
//    { 31, 6, { 3, 0, 0, 0 } }, // FT_COAL: 3
//    { 32, 6, { 0, 3, 0, 0 } }, // FT_HEATING_OIL: 3
//    { 33, 4, { 0, 0, 0, 0 } }, // FT_NONE
//    { 34, 5, { 0, 0, 0, 1 } }, // FT_URANIUM: 1
//    { 35, 5, { 0, 1, 0, 0 } }, // FT_HEATING_OIL: 1
//    { 36, 7, { 3, 0, 0, 0 } }, // FT_COAL: 3
//    { 37, 4, { 0, 0, 0, 0 } }, // FT_NONE
//    { 38, 7, { 0, 0, 3, 0 } }, // FT_WASTE: 3
//    { 39, 6, { 0, 0, 0, 1 } }, // FT_URANIUM: 1
//    { 40, 6, { 0, 2, 0, 0 } }, // FT_HEATING_OIL: 2
//    { 42, 6, { 2, 0, 0, 0 } }, // FT_COAL: 2
//    { 44, 5, { 0, 0, 0, 0 } }, // FT_NONE
//    { 46, 7, { 3, 3, 0, 0 } }, // FT_HYBRID: 3
//    { 50, 6, { 0, 0, 0, 0 } }, // FT_NONE
//};

//enum Consts
//{
//    CITIES_AMOUNT = sizeof(cities) / sizeof(cities[0]),
//    POWER_LINES_AMOUNT = sizeof(powerLines) / sizeof(powerLines[0]),
//    POWER_STATIONS_AMOUNT = sizeof(powerStations) / sizeof(powerStations[0]),
//};


//uint64_t getWideRandomValue()
//{
//    return (((uint64_t)rand()) << 32) | (uint64_t)rand();
//}

//void milliSleep(unsigned long ms)
//{
//    struct timespec required, remaining;
//    int res;
//    remaining.tv_sec = ms / 1000;
//    remaining.tv_nsec = (ms % 1000) * 1000000;
//    do
//    {
//        required = remaining;
//        res = nanosleep(&required, &remaining);
//    } while ( res != 0 && (res != -1 || errno == EINTR) );
//}

//uint64_t getMillisecondsSinceEpoch()
//{
//    struct timeval tv;
//    gettimeofday(&tv, NULL);
//    return (uint64_t)(tv.tv_sec) * 1000 + (uint64_t)(tv.tv_usec) / 1000;
//}

/* The function requires the 'str' argument is to be able to fit at least 8 bytes */
//void protocolVersionToString(uint16_t protocolVersion, char *str)
//{
//    assert(str);
//    sprintf(str, "%u.%u", protocolVersion >> 8, protocolVersion & 0xFF);
//}

#ifdef _LOGGING
void logPrintf(const char * format, ...)
{
    assert(format);
    va_list args;
    va_start (args, format);
    vfprintf (stderr, format, args);
    va_end (args);
}
#endif // _LOGGING
