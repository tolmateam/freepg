#include "networking.h"
#include "status-response.h"
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <wctype.h>
#include <string.h>


static bool _isNicknameCorrect(const wchar_t *nickname);
static void _hashPassword(uint8_t *hash, const wchar_t *password);
static ErrorCauses _respondToStatusPoll(ClientHandle *handle, const StatusPoll *sp);
static void _handleStatusResponse(ServerHandle *handle, const StatusResponse *sr);

typedef enum protocol_version_t
{
    VERSION_MAJOR = 0,
    VERSION_MINOR = 1,
} ProtocolVersion;

enum LocalConsts
{
    SERVER_ADDRESS_MAX_LENGTH = 32,
    GAMEPLAY_TCP_PORT = 5670,
};

static bool _isNicknameCorrect(const wchar_t *nickname)
{
    if(!nickname)
        return false;
    size_t length = getWideStringLength(nickname);
    if(length < NICKNAME_MIN_LENGTH || length > NICKNAME_MAX_LENGTH)
        return false;
    for(size_t i = 0; i < length; i++)
        if(iswcntrl(nickname[i]))
            return false;
    return true;
}

static void _hashPassword(uint8_t *hash, const wchar_t *password)
{
    assert(hash && password);
    int index = 0;
    size_t length = getWideStringLength(password);
    memset(hash, 'A', PASSWORD_HASH_LENGTH);
    if(length == 0)
        return;
    for(int i = 0; i < PASSWORD_HASH_LENGTH; i++, index = (index+1) % length)
    {
        int temp = password[index] % 16;
        hash[i] = temp < 10 ? temp + '0' : temp + 'A' - 1;
    }
}

static ErrorCauses _respondToStatusPoll(ClientHandle *handle, const StatusPoll *sp)
{
    assert(handle);
    assert(sp);

    // Consider all right by default
    ErrorCauses cause = OK_CAUSE;

    handle->gameState.protocolVersion = sp->protocolVersion;
    if(getLocalProtocolVersion() != sp->protocolVersion)
        cause = PROTOCOL_VERSION_CAUSE;

    size_t len = getWideStringLength(handle->nickname);
    for(int i = 0; i < sp->disconnectPlayersAmount; i++)
    {
        if(wcsncmp(handle->nickname, sp->disconnectedPlayers[i].nickname, len) == 0)
        {
            logPrintf("Server has disconnected us due to cause %d\n", sp->disconnectedPlayers[i].cause);
            cause = CONNECTION_CAUSE;
            handle->gameState.connected = false;
//            goto fin;
        }
    }

    // Try to find itself as a player
    size_t index = (size_t)-1;
    handle->gameState.playersAmount = sp->playersAmount;
    for(size_t i = 0; i < sp->playersAmount; i++)
    {
        if(wcsncmp(handle->nickname, sp->players[i].nickname, len) == 0)
            index = i;
        wcsncpy(handle->gameState.players[i].nickname, sp->players[i].nickname, len);
        handle->gameState.players[i].ready = sp->players[i].ready;
        logPrintf("%lc %ls %c\n",
                 i == index ? L'>' : L' ', sp->players[i].nickname, sp->players[i].ready ? '+' : '-');
    }
    handle->gameState.myPlayer = index;

    bool actionRequired = (index == (size_t)-1) ? true: sp->players[index].actionRequired;
    handle->gameState.gameStarted = false;

//    if(!actionRequired)
//        goto fin;

    void *out_msg = NULL;
    size_t size = createStatusResponseMsg(handle, &out_msg);
    if((long)size < 0)
    {
        cause = INTERNAL_CAUSE;
        goto fin;
    }

    if((size_t)nn_send(handle->socket, out_msg, size, 0) != size)
    {
        logMessage("Failed to send StatusResponse", out_msg, size);
        cause = CONNECTION_CAUSE;
    }
    else
        logMessage("Send StatusRespose", out_msg, size);

    free(out_msg);

fin:
    freeStatusPoll(sp);
    return cause;
}

static void _handleStatusResponse(ServerHandle *handle, const StatusResponse *sr)
{
    assert(handle);
    assert(sr);

    // Who sent the message?
    const size_t len = getWideStringLength(sr->playerCredentials.nickname);
    PlayerData *player = NULL;
    for(size_t i = 0; i < handle->playersAmount; i++)
    {
        if(wcsncmp(sr->playerCredentials.nickname, handle->players[i].nickname, len) == 0)
        {
            player = &handle->players[i];
            break;
        }
    }

    if(player) // Is the player known?
    {
        // We know the player. Check his or her credentials.
        if(memcmp(sr->playerCredentials.password, player->password, PASSWORD_HASH_LENGTH) != 0)
            return; // Forget this message

        // The response is correct. Take it into account.
        player->respond = true;
        player->networkErrorCounter = 0;
        // TODO. Migrate to action-type/action-items
        //player->ready = sr->ready;
        return;
    }

    ServerRejectCauses cause = OK_REJECT_CAUSE;

    // We don't know he or she. They are a newbie. Check their name.
    if(_isNicknameCorrect(sr->playerCredentials.nickname))
    {
        // They are a newbie and their name is correct.
        // Let them welcome if we have some place for them around the playing table.
        if(handle->playersAmount < MAX_PLAYERS)
        {
            // We really have! Let them play!
            player = &handle->players[handle->playersAmount++];
            wcsncpy(player->nickname, sr->playerCredentials.nickname, len+1);
            memcpy(player->password, sr->playerCredentials.password, PASSWORD_HASH_LENGTH);
            player->networkErrorCounter = 0;
            player->respond = true;
            // TODO. Migrate to action-type/action-items
            //player->ready = sr->ready;
            player->aiHandle = NULL;
            return;
        }

        // Our bunch is full! Sorry!
        cause = EXTRA_PLAYER_CAUSE;
    }
    else
        cause = INVALID_NICKNAME_CAUSE;

    if(handle->playersToDisconnectAmount < MAX_PLAYERS_TO_DISCONNECT)
    {
        // Add disconnect information to appropriate data collection
        DisconnectedPlayer *dp = &handle->playersToDisconnect[handle->playersToDisconnectAmount++];
        dp->cause = cause;
        wcsncpy(dp->nickname, sr->playerCredentials.nickname, len+1);
    }
}


//-------------------------------
// Common interface functions
//-------------------------------

uint16_t getLocalProtocolVersion()
{
    return ((uint8_t) VERSION_MAJOR << 8) | (uint8_t) VERSION_MINOR;
}


//-------------------------------
// Interface functions for client
//-------------------------------

ErrorCauses startClient(ClientHandle *handle, const char *serverAddress, const wchar_t *nickname, const wchar_t *password)
{
    assert(handle);

    int tcpNoDelay = 1;
    int sendTimeout = REGULAR_SEND_TIMEOUT;
    int recvTimeout = SHORT_RECEIVE_TIMEOUT;
    char buffer[SERVER_ADDRESS_MAX_LENGTH*2];

    if(!handle || !serverAddress || !_isNicknameCorrect(nickname) || !password || getWideStringLength(password) < 1)
        return INVALID_ARGUMENT_CAUSE;
    size_t length = strlen(serverAddress);
    if(length == 0 || length >= SERVER_ADDRESS_MAX_LENGTH)
        return INVALID_ARGUMENT_CAUSE;

    memset(handle, 0, sizeof(ClientHandle));

    // Setting up Survey socket
    handle->socket = nn_socket(AF_SP, NN_RESPONDENT);
    if(handle->socket < 0)
        return CONNECTION_CAUSE;
    if(nn_setsockopt(handle->socket, NN_TCP, NN_TCP_NODELAY, &tcpNoDelay, sizeof(tcpNoDelay)) != 0)
        return CONNECTION_CAUSE;
    if(nn_setsockopt(handle->socket, NN_SOL_SOCKET, NN_SNDTIMEO, &sendTimeout, sizeof(sendTimeout)))
        return CONNECTION_CAUSE;
    if(nn_setsockopt(handle->socket, NN_SOL_SOCKET, NN_RCVTIMEO, &recvTimeout, sizeof(recvTimeout)))
        return CONNECTION_CAUSE;
    sprintf(buffer, "tcp://%s:%d", serverAddress, GAMEPLAY_TCP_PORT);
    handle->connection = nn_connect(handle->socket, buffer);
    if(handle->connection < 0)
        return CONNECTION_CAUSE;

    wcsncpy(handle->nickname, nickname, getWideStringLength(nickname)+1);
    _hashPassword(handle->password, password);
    handle->action = READY_ACTION;
    handle->actionItemsAmount = 0;
    
    handle->gameState.connected = false;
    handle->gameState.playersAmount = 0;
    handle->gameState.myPlayer = (size_t)-1;
    handle->gameState.gameStarted = false;
    handle->initialized = true;

    uint64_t deadline = getMillisecondsSinceEpoch() + CONNECTION_TIMEOUT;
    ErrorCauses res;
    while((res = respondToServer(handle)) == OK_CAUSE && !handle->gameState.connected && getMillisecondsSinceEpoch() < deadline)
        milliSleep(50);

    if(!handle->gameState.connected && res == OK_CAUSE)
        res = CONNECTION_CAUSE;
    return res;
}

void stopClient(ClientHandle *handle)
{
    if(!handle)
        return;
    if(handle->initialized && handle->socket >= 0)
    {
        if(handle->connection >= 0)
            nn_shutdown(handle->socket, handle->connection);
        nn_close(handle->socket);
    }
    memset(handle, 0, sizeof(ClientHandle));
}

ErrorCauses respondToServer(ClientHandle *handle)
{
    assert(handle && handle->initialized);

    void *msg = NULL;

    // Get server poll
    int amount = nn_recv(handle->socket, &msg, NN_MSG, NN_DONTWAIT);
    if(amount < 0)
    {
        if(nn_errno() != EAGAIN)
        {
            logPrintf("Failed to receive a message\n");
            return CONNECTION_CAUSE;
        }
        return OK_CAUSE;
    }
    logMessage("Got incoming message", msg, (size_t)amount);

    MessageHeader mh;
    if(!parseMsgHeader(&mh, msg))
        return CONNECTION_CAUSE;
    handle->sequenceNumber = mh.sequenceNumber;

    ErrorCauses cause = OK_CAUSE;
    switch(mh.id)
    {
    case STATUS_POLL_MESSAGE_ID:
        {
            StatusPoll sp;
//            cause = parseStatusPollMsg(&sp, mh.length, msg + MESSAGE_HEADER_SIZE)
//                    ? _respondToStatusPoll(handle, &sp)
//                    : CONNECTION_CAUSE;
        }
        break;
////    case MOVE_POLL_MESSAGE_ID:
////        cause = parseMovePollMsg(&sp, mh.length, in_msg + MESSAGE_HEADER_SIZE)
////                ? _respondToStatusPoll(handle, &sp)
////                : CONNECTION_CAUSE;
////        break;
////    case GAMEOVER_POLL_MESSAGE_ID:
////        cause = parseGameoverPollMsg(&sp, mh.length, in_msg + MESSAGE_HEADER_SIZE)
////                ? _respondToStatusPoll(handle, &sp)
////                : CONNECTION_CAUSE;
////        break;
    default:
        logPrintf("Message ID is unknown (%d)\n", mh.id);
        cause = CONNECTION_CAUSE;
        break;
    }

    nn_freemsg(msg);
    if(cause == OK_CAUSE)
        handle->gameState.connected = true;
    return cause;
}

const GameState *getGameState(const ClientHandle *handle)
{
    assert(handle && handle->initialized);
    return &handle->gameState;
}

void setClientReady(ClientHandle *handle, bool ready)
{
    assert(handle && handle->initialized);
    handle->action = READY_ACTION;
    handle->actionItemsAmount = 1;
    handle->actionItemsContent[0] = (uint16_t)ready;
}


//-------------------------------
// Interface functions for server
//-------------------------------

ErrorCauses startServer(ServerHandle *handle)
{
    assert(handle);

    int tcpNoDelay = 1;
    int sendTimeout = REGULAR_SEND_TIMEOUT;
    int recvTimeout = REGULAR_RECEIVE_TIMEOUT;
    int surveyDeadline = SURVEY_DEADLINE;
    char bindAddress[SERVER_ADDRESS_MAX_LENGTH];

    if(!handle)
        return INVALID_ARGUMENT_CAUSE;
    memset(handle, 0, sizeof(ServerHandle));

    // Setting up Survey socket
    handle->socket = nn_socket(AF_SP, NN_SURVEYOR);
    if(handle->socket < 0)
        return CONNECTION_CAUSE;
    if(nn_setsockopt(handle->socket, NN_TCP, NN_TCP_NODELAY, &tcpNoDelay, sizeof(tcpNoDelay)))
        return CONNECTION_CAUSE;
    if(nn_setsockopt(handle->socket, NN_SOL_SOCKET, NN_SNDTIMEO, &sendTimeout, sizeof(sendTimeout)))
        return CONNECTION_CAUSE;
    if(nn_setsockopt(handle->socket, NN_SOL_SOCKET, NN_RCVTIMEO, &recvTimeout, sizeof(recvTimeout)))
        return CONNECTION_CAUSE;
    if(nn_setsockopt(handle->socket, NN_SURVEYOR, NN_SURVEYOR_DEADLINE, &surveyDeadline, sizeof(surveyDeadline)))
        return CONNECTION_CAUSE;
    memset(bindAddress, 0, SERVER_ADDRESS_MAX_LENGTH);
    sprintf(bindAddress, "tcp://*:%d", GAMEPLAY_TCP_PORT);
    handle->endpoint = nn_bind(handle->socket, bindAddress);
    if(handle->endpoint < 0)
        return CONNECTION_CAUSE;

    handle->sequenceNumber = rand() & 0x0000FFFF;
    handle->playersAmount = 0;
    handle->gameStage = BEFORE_GAME_STAGE;
    handle->initialized = true;

    return OK_CAUSE;
}

void stopServer(ServerHandle *handle)
{
    if(!handle)
        return;
    if(handle->initialized && handle->socket >= 0)
    {
        if(handle->endpoint >= 0)
            nn_shutdown(handle->socket, handle->endpoint);
        nn_close(handle->socket);
    }
    memset(handle, 0, sizeof(ServerHandle));
}

ErrorCauses pollingClients(ServerHandle *handle)
{
    assert(handle && handle->initialized);
    if(!handle)
        return INVALID_ARGUMENT_CAUSE;

    void *msg = NULL;

    advanceSequenceNumber(&handle->sequenceNumber);
    size_t size = createStatusPollMsg(handle, &msg);

    if(nn_send(handle->socket, msg, size, 0) != (int)size)
    {
        logMessage("Failed to send StatusPoll", msg, size);
        return CONNECTION_CAUSE;
    }
    logMessage("Send StatusPoll", msg, size);
    free(msg);

    // Clear reported information about disconnected players
    handle->playersToDisconnectAmount = 0;

    // Reset respond flags
    for(size_t i = 0; i < handle->playersAmount; i++)
        handle->players[i].respond = false;

    // Get players' responses
    for( ; true; nn_freemsg(msg))
    {
        // Receive response from a player
        int amount = nn_recv(handle->socket, &msg, NN_MSG, 0);
        logPrintf("nn_recv() returns %d\n", amount);
        if(amount < 0)
        {
            if(nn_errno() != EFSM)
            {
                logPrintf("Failed to receive survey response\n");
                return CONNECTION_CAUSE;
            }
            logPrintf("The survey is timed out\n");
            break;
        }
        logMessage("Got incoming message", msg, (size_t)amount);

        // Parse message header
        MessageHeader mh;
        if(!parseMsgHeader(&mh, msg))
            continue;
        if(mh.sequenceNumber != handle->sequenceNumber)
            continue;

        switch(mh.id)
        {
        case STATUS_RESPONSE_MESSAGE_ID:
            {
                StatusResponse sr;
//                if(parseStatusResponseMsg(&sr, mh.length, msg + MESSAGE_HEADER_SIZE))
//                    _handleStatusResponse(handle, &sr);
                freeStatusResponse(&sr);
            }
            break;
        default:
            break;
        }
    }

    // The time is out! Client responses are in the handle.
    return OK_CAUSE;
}
