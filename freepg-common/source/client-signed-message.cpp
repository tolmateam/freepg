#include <freepg-common/client-signed-message.h>
#include <assert.h>


//------------------
// Static functions
//------------------

/*static */uint16_t ClientSignedMessage::calculateLength(const QString &nickname, const QByteArray &password_hash)
{
    return sizeof(uint16_t) + static_cast<uint16_t>(nickname.toUtf8().size())
            + static_cast<uint16_t>(password_hash.size());
}


//--------------------------
// Construction/destruction
//--------------------------

ClientSignedMessage::ClientSignedMessage(uint16_t length, const QString &nickname, const QByteArray &password_hash)
    : Message(length + calculateLength(nickname, password_hash))
    , _nickname(nickname)
    , _password_hash(password_hash)
{
    assert(nickname.length() >= GameConsts::NICKNAME_MIN_LENGTH);
    assert(nickname.length() <= GameConsts::NICKNAME_MAX_LENGTH);
    assert(nickname.toUtf8().size() <= GameConsts::NICKNAME_MAX_SIZE);
    assert(password_hash.size() == GameConsts::PASSWORD_HASH_SIZE);
}


//-------------------------------
// Serialization/deserialization
//-------------------------------

uint16_t ClientSignedMessage::storageSize() const
{
    return Message::storageSize() + calculateLength(_nickname, _password_hash);
}

QByteArray ClientSignedMessage::serialize() const
{
    QByteArray buffer = Message::serialize();
    buffer.reserve(static_cast<int>(ClientSignedMessage::storageSize()));
    QByteArray nickname_utf8 = _nickname.toUtf8();
    return buffer.append(Message::storeUint16(static_cast<uint16_t>(nickname_utf8.size())))
            .append(_nickname.toUtf8())
            .append(_password_hash);
}

bool ClientSignedMessage::deserialize(const QByteArray &buffer)
{
    size_t size = static_cast<size_t>(buffer.size());
    const char *data = buffer.data();

    if (size < Message::storageSize())
        return false;
    if (!Message::deserialize(buffer))
        return false;
    data += Message::storageSize();
    size -= Message::storageSize();

    if (size < sizeof(uint16_t))
        return false;
    uint16_t nick_size = restoreUint16(data);
    data += sizeof(nick_size);
    size -= sizeof(nick_size);
    if (nick_size > GameConsts::NICKNAME_MAX_SIZE)
        return false;

    if (size < nick_size)
        return false;
    _nickname = QString().fromUtf8(data, static_cast<int>(nick_size));
    if (_nickname.length() < GameConsts::NICKNAME_MIN_LENGTH
            || _nickname.length() > GameConsts::NICKNAME_MAX_LENGTH) {
        return false;
    }
    data += nick_size;
    size -= nick_size;

    if (size < GameConsts::PASSWORD_HASH_SIZE)
        return false;
    _password_hash = QByteArray(data, GameConsts::PASSWORD_HASH_SIZE);
    data += GameConsts::PASSWORD_HASH_SIZE;
    size -= GameConsts::PASSWORD_HASH_SIZE;

    return true;
}
