#include <freepg-common/status-poll.h>
#include <assert.h>


//------------------
// Static functions
//------------------

/*static */uint16_t StatusPollMessage::calculateLength()
{
    return sizeof(uint8_t);
}


//--------------------------
// Construction/destruction
//--------------------------

StatusPollMessage::StatusPollMessage(const Stages &stage)
    : Message(calculateLength())
    , _stage(stage)
{
    assert(stage >= Stages::MIN_STAGE && stage < Stages::MAX_STAGE);
}


//-------------------------------
// Serialization/deserialization
//-------------------------------

uint16_t StatusPollMessage::storageSize() const
{
    return Message::storageSize() + calculateLength();
}

QByteArray StatusPollMessage::serialize() const
{
    QByteArray buffer = Message::serialize();
    buffer.reserve(static_cast<int>(StatusPollMessage::storageSize()));
    return buffer.append(Message::storeUint8(_stage));
}

bool StatusPollMessage::deserialize(const QByteArray &buffer)
{
    size_t size = static_cast<size_t>(buffer.size());
    const char *data = buffer.data();

    if (size < Message::storageSize())
        return false;
    if (!Message::deserialize(buffer))
        return false;
    data += Message::storageSize();
    size -= Message::storageSize();

    if (size < sizeof(uint8_t))
        return false;
    _stage = static_cast<Stages>(Message::restoreUint8(data));

    data += sizeof(uint8_t);
    size -= sizeof(uint8_t);

    return checkLength();
}

QString StatusPollMessage::stageString() const
{
    switch (_stage) {
    case Stages::BEFORE_GAME:
        return "BeforeGame";
    case Stages::STAGE_ONE:
        return "Stage1";
    case Stages::STAGE_TWO:
        return "Stage2";
    case Stages::STAGE_THREE:
        return "Stage3";
    case Stages::GAME_OVER:
        return "GameOver";
    default:
        break;
    }
    return "UnknownStage";
}
