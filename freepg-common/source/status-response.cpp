#include <freepg-common/status-response.h>
#include <assert.h>


//------------------
// Static functions
//------------------

/*static */uint16_t StatusResponseMessage::calculateLength()
{
    return sizeof(uint8_t);
}


//--------------------------
// Construction/destruction
//--------------------------

StatusResponseMessage::StatusResponseMessage(
        const QString &nickname, const QByteArray &password_hash, bool ready)
    : ClientSignedMessage(calculateLength(), nickname, password_hash)
    , _ready(ready)
{
}


//-------------------------------
// Serialization/deserialization
//-------------------------------

uint16_t StatusResponseMessage::storageSize() const
{
    return ClientSignedMessage::storageSize() + calculateLength();
}

QByteArray StatusResponseMessage::serialize() const
{
    QByteArray buffer = ClientSignedMessage::serialize();
    buffer.reserve(static_cast<int>(StatusResponseMessage::storageSize()));
    return buffer.append(Message::storeUint8(_ready));
}

bool StatusResponseMessage::deserialize(const QByteArray &buffer)
{
    size_t size = static_cast<size_t>(buffer.size());
    const char *data = buffer.data();

    if (size < ClientSignedMessage::storageSize())
        return false;
    if (!ClientSignedMessage::deserialize(buffer))
        return false;
    data += ClientSignedMessage::storageSize();
    size -= ClientSignedMessage::storageSize();

    if (size < sizeof(uint8_t))
        return false;
    _ready = (Message::restoreUint8(data) != 0);

    data += sizeof(uint8_t);
    size -= sizeof(uint8_t);

    return checkLength();
}
