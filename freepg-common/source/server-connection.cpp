#include <freepg-common/server-connection.h>
#include <freepg-common/join-request.h>
#include <freepg-common/join-confirm.h>
#include <freepg-common/join-reject.h>
#include <freepg-common/client-signed-message.h>
#include <freepg-common/message.h>
#include <freepg-common/types.h>
#include <QUdpSocket>
#include <QCryptographicHash>
#include <QDateTime>
#include <assert.h>
#include <stdlib.h>


ClientHandle::ClientHandle(const QString &nickname, const SocketAddress &ip)
    : _nickname(nickname)
    , _password_hash()
    , _ip(ip)
    , _send_sequence_number(0)
    , _receive_sequence_number(0)
{
    _error = (_nickname.length() >= NICKNAME_MIN_LENGTH && _nickname.length() <= NICKNAME_MAX_LENGTH)
            ? Connection::NO_ERROR : Connection::ILLEGAL_NICKNAME_FORMAT;
    assert(_error == Connection::NO_ERROR);
}

void ClientHandle::setPasswordHash()
{
    assert(isOk());
    if (!isOk())
        return;

    QString to_hash = QString("%1%2%3")
            .arg(_nickname)
            .arg(QDateTime::currentMSecsSinceEpoch())
            .arg(rand());
    _password_hash = QCryptographicHash::hash(to_hash.toUtf8(), QCryptographicHash::Md4);
    assert(_password_hash.size() == PASSWORD_HASH_SIZE);
}



ServerConnection::ServerConnection()
    : Connection(Connection::Consts::SERVER_PORT)
{
}

ssize_t ServerConnection::clientIndexByNickname(const QString &nickname)
{
    for (ConstClientHandleIterator it = _client_handles.cbegin(); it != _client_handles.cend(); it++) {
        if (it->nickname() == nickname)
            return (it - _client_handles.cbegin());
    }
    return -1;
}

void ServerConnection::incomingMessageHandler(const SocketAddress &sender, const Message *message)
{
    assert(message);

    if (sender.address().isNull() || sender.port() == 0) {
        // Ignore the message from unknown host
        setError(Connection::Errors::ILLEGAL_MESSAGE_SENDER);
        return;
    }

    switch (message->id()) {
    case Message::Ids::JOIN_REQUEST:
        // Call a special handler for Join Request message
        incomingJoinRequestHandler(sender, static_cast<const JoinRequestMessage *>(message));
        return;

    case Message::Ids::STATUS_RESPONSE:
        break;

    default:
        setError(Errors::ILLEGAL_TYPE_OF_RECEIVED_MESSAGE);
        return;
    }

    const ClientSignedMessage *msg = static_cast<const ClientSignedMessage *>(message);

    ssize_t index = clientIndexByNickname(msg->nickname());
    if (index < 0) {
        // Ignore non-join-request message from unknown host
        setError(Connection::Errors::ILLEGAL_MESSAGE_SENDER);
        return;
    }
    assert(static_cast<size_t>(index) < _client_handles.size());
    ClientHandle &client = _client_handles[index];

    if (client._password_hash != msg->passwordHash()) {
        client.setError(Connection::Errors::WRONG_PASSWORD);
        setError(client.error());
    }

    size_t sequence_number_delta = (client._receive_sequence_number - message->sequenceNumber()) & 0xFFFF;
    if (sequence_number_delta > Connection::Consts::MISSED_DATAGRAM_CRITICAL_COUNT) {
        client.setError(Connection::Errors::MULTIPLE_MESSAGES_MISSED);
        setError(Connection::Errors::MULTIPLE_MESSAGES_MISSED);
        return;
    } else if (sequence_number_delta > 0) {
        client.setError(Connection::Errors::MISSED_MESSAGES_DETECTED);
        setError(Connection::Errors::MISSED_MESSAGES_DETECTED);
        client._receive_sequence_number = message->sequenceNumber();
    }
    Connection::advanceSequenceNumber(client._receive_sequence_number);

    emit incomingMessage(client, message);
}

void ServerConnection::incomingJoinRequestHandler(const SocketAddress &sender, const JoinRequestMessage *message)
{
    assert(message);

    ssize_t index = clientIndexByNickname(message->nickname());
    if (index >= 0) {
        // Ignore join-request message if have already received one
        assert(static_cast<size_t>(index) < _client_handles.size());
        setError(Connection::Errors::JOIN_REQUEST_DUPLICATED);
        _client_handles[index].setError(error());
        return;
    }

    ClientHandle client(message->nickname(), sender);
    client._receive_sequence_number = message->sequenceNumber();
    Connection::advanceSequenceNumber(client._receive_sequence_number);

    emit clientJoinRequest(client);
}

void ServerConnection::acceptClient(const ClientHandle &client_handle)
{
    ssize_t index = clientIndexByNickname(client_handle.nickname());
    assert(index < 0);
    _client_handles.push_back(client_handle);
    _client_handles.back().setPasswordHash();
    JoinConfirmMessage msg(_client_handles.back()._password_hash);
    sendToClient(_client_handles.back(), msg);
}

void ServerConnection::declineClient(const ClientHandle &client_handle)
{
    ssize_t index = clientIndexByNickname(client_handle.nickname());
    assert(index < 0);
    JoinRejectMessage msg(JoinRejectMessage::Causes::NO_ROOM_TO_PLAY);
    sendToClient(client_handle, msg);
}

void ServerConnection::removeClient(const QString &nickname)
{
    ssize_t index = clientIndexByNickname(nickname);
    assert(index >= 0);
    _client_handles.erase(_client_handles.cbegin() + index);
}

bool ServerConnection::sendToClient(const ClientHandle &client_handle, Message &message)
{
    switch (message.id()) {
    case Message::Ids::JOIN_CONFIRM:
    case Message::Ids::JOIN_REJECT:
    case Message::Ids::STATUS_POLL:
        break;

    default:
        setError(Errors::ILLEGAL_MESSAGE_TYPE_TO_SEND);
        return false;
    }

    message.setSequenceNumber(client_handle._send_sequence_number);
    if (!sendTo(message, client_handle._ip))
        return false;

    ClientHandle &mutable_handle = const_cast<ClientHandle &>(client_handle);
    Connection::advanceSequenceNumber(mutable_handle._send_sequence_number);
    return true;
}
