#include <freepg-common/message.h>
#include <assert.h>


//--------------------------
// Construction/destruction
//--------------------------

Message::Message(uint16_t length)
    : _length(length + sizeof(_sequenceNumber) + sizeof(uint8_t))
    , _sequenceNumber(0)
{ }

Message::Message()
    : _length(0)
    , _sequenceNumber(0)
{ }

/*virtual */Message::~Message() { }


//-------------------------------
// Serialization/deserialization
//-------------------------------

/*static */Message::Ids Message::readIdFromBuffer(const QByteArray &buffer)
{
    if (buffer.size() < Message::minStorageSize())
        return Message::Ids::UNKNOWN;
    return static_cast<Message::Ids>(
                restoreUint8(buffer.data() + sizeof(_length) + sizeof(_sequenceNumber)));
}

/*static */uint16_t Message::minStorageSize()
{
    return sizeof(_length)
            + sizeof(_sequenceNumber)
            + sizeof(uint8_t); // id
}

/*virtual */uint16_t Message::storageSize() const
{
    return Message::minStorageSize();
}

/*virtual */QByteArray Message::serialize() const
{
    QByteArray buffer;
    buffer.reserve(static_cast<int>(Message::storageSize()));
    return buffer.append(Message::storeUint16(_length))
            .append(Message::storeUint16(_sequenceNumber))
            .append(Message::storeUint8(id()));
}

/*virtual */bool Message::deserialize(const QByteArray &buffer)
{
    size_t size = static_cast<size_t>(buffer.size());
    const char *data = buffer.data();

    if (size < sizeof(_length))
        return false;
    _length = Message::restoreUint16(data);
    if (_length < Message::storageSize() - sizeof(_length))
        return false;
    data += sizeof(_length);
    size -= sizeof(_length);

    if (size < sizeof(_sequenceNumber))
        return false;
    _sequenceNumber = Message::restoreUint16(data);
    data += sizeof(_sequenceNumber);
    size -= sizeof(_sequenceNumber);

    if (size < sizeof(uint8_t))
        return false;
    uint8_t message_id = Message::restoreUint8(data);
    return message_id == id();
}


//------------------------------------------------
// Serialization of the simple data to the buffer
//------------------------------------------------

/*static */QByteArray Message::storeUint8(uint8_t value)
{
    return QByteArray().append(value);
}

/*static */QByteArray Message::storeUint16(uint16_t value)
{
    QByteArray buffer;
    buffer.reserve(2);
    return buffer.append((value >> 8) & 0xFF)
            .append(value & 0xFF);
}

/*static */QByteArray Message::storeUint32(uint32_t value)
{
    QByteArray buffer;
    buffer.reserve(4);
    return buffer.append((value >> 24) & 0xFF)
            .append((value >> 16) & 0xFF)
            .append((value >> 8) & 0xFF)
            .append(value & 0xFF);
}

/*static */QByteArray Message::storeInt8(int8_t value)
{
    return Message::storeUint8(static_cast<uint8_t>(value));
}

/*static */QByteArray Message::storeInt16(int16_t value)
{
    return Message::storeUint16(static_cast<uint16_t>(value));
}

/*static */QByteArray Message::storeInt32(int32_t value)
{
    return Message::storeUint32(static_cast<uint32_t>(value));
}


//----------------------------------------------------
// Deserialization of the simple data from the buffer
//----------------------------------------------------

/*static */uint8_t Message::restoreUint8(const char *buffer)
{
    return static_cast<uint8_t>(buffer[0]);
}

/*static */uint16_t Message::restoreUint16(const char *buffer)
{
    return static_cast<uint16_t>(buffer[0] << 8)
            | static_cast<uint16_t>(buffer[1]);
}

/*static */uint32_t Message::restoreUint32(const char *buffer)
{
    return static_cast<uint32_t>(buffer[0] << 24)
            | static_cast<uint32_t>(buffer[1] << 16)
            | static_cast<uint32_t>(buffer[2] << 8)
            | static_cast<uint32_t>(buffer[3]);
}

/*static */int8_t Message::restoreInt8(const char *buffer)
{
    return static_cast<int8_t>(Message::restoreUint8(buffer));
}

/*static */int16_t Message::restoreInt16(const char *buffer)
{
    return static_cast<int16_t>(Message::restoreUint16(buffer));
}

/*static */int32_t Message::restoreInt32(const char *buffer)
{
    return static_cast<int32_t>(Message::restoreUint32(buffer));
}
