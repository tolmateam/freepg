#include <freepg-common/join-confirm.h>
#include <assert.h>


//------------------
// Static functions
//------------------

/*static */uint16_t JoinConfirmMessage::calculateLength()
{
    return static_cast<uint16_t>(GameConsts::PASSWORD_HASH_SIZE);
}


//--------------------------
// Construction/destruction
//--------------------------

JoinConfirmMessage::JoinConfirmMessage(const QByteArray &password_hash)
    : Message(calculateLength())
    , _password_hash(password_hash)
{
    assert(password_hash.size() == GameConsts::PASSWORD_HASH_SIZE);
}


//-------------------------------
// Serialization/deserialization
//-------------------------------

uint16_t JoinConfirmMessage::storageSize() const
{
    return Message::storageSize() + calculateLength();
}

QByteArray JoinConfirmMessage::serialize() const
{
    QByteArray buffer = Message::serialize();
    buffer.reserve(static_cast<int>(JoinConfirmMessage::storageSize()));
    return buffer.append(_password_hash);
}

bool JoinConfirmMessage::deserialize(const QByteArray &buffer)
{
    size_t size = static_cast<size_t>(buffer.size());
    const char *data = buffer.data();

    if (size < Message::storageSize())
        return false;
    if (!Message::deserialize(buffer))
        return false;
    data += Message::storageSize();
    size -= Message::storageSize();

    if (size < GameConsts::PASSWORD_HASH_SIZE)
        return false;
    _password_hash = QByteArray(data, GameConsts::PASSWORD_HASH_SIZE);
    data += GameConsts::PASSWORD_HASH_SIZE;
    size -= GameConsts::PASSWORD_HASH_SIZE;

    return checkLength();
}
