#include <freepg-common/join-reject.h>
#include <assert.h>


//------------------
// Static functions
//------------------

/*static */uint16_t JoinRejectMessage::calculateLength()
{
    return sizeof(uint8_t);
}


//--------------------------
// Construction/destruction
//--------------------------

JoinRejectMessage::JoinRejectMessage(const Causes &cause)
    : Message(calculateLength())
    , _cause(cause)
{
    assert(cause >= Causes::MIN_CAUSE && cause < Causes::MAX_CAUSE);
}


//-------------------------------
// Serialization/deserialization
//-------------------------------

uint16_t JoinRejectMessage::storageSize() const
{
    return Message::storageSize() + calculateLength();
}

QByteArray JoinRejectMessage::serialize() const
{
    QByteArray buffer = Message::serialize();
    buffer.reserve(static_cast<int>(JoinRejectMessage::storageSize()));
    return buffer.append(Message::storeUint8(_cause));
}

bool JoinRejectMessage::deserialize(const QByteArray &buffer)
{
    size_t size = static_cast<size_t>(buffer.size());
    const char *data = buffer.data();

    if (size < Message::storageSize())
        return false;
    if (!Message::deserialize(buffer))
        return false;
    data += Message::storageSize();
    size -= Message::storageSize();

    if (size < sizeof(uint8_t))
        return false;
    _cause = static_cast<Causes>(Message::restoreUint8(data));

    data += sizeof(uint8_t);
    size -= sizeof(uint8_t);

    return checkLength();
}
