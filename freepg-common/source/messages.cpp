#include <freepg-common/join-confirm.h>
#include <freepg-common/join-reject.h>
#include <freepg-common/join-request.h>
#include <freepg-common/status-poll.h>
#include <freepg-common/status-response.h>

/*static */Message *Message::createMessageFromBuffer(const QByteArray &buffer)
{
    Message *msg = nullptr;

    switch (Message::readIdFromBuffer(buffer)) {
    case Message::Ids::JOIN_CONFIRM:
        msg = new JoinConfirmMessage;
        break;

    case Message::Ids::JOIN_REJECT:
        msg = new JoinRejectMessage;
        break;

    case Message::Ids::JOIN_REQUEST:
        msg = new JoinRequestMessage;
        break;

    case Message::Ids::STATUS_POLL:
        msg = new StatusPollMessage;
        break;

    case Message::Ids::STATUS_RESPONSE:
        msg = new StatusResponseMessage;
        break;
//    case Message::Ids::MOVE_POLL:
//    case Message::Ids::MOVE_RESPONSE:
//    case Message::Ids::GAMEOVER_POLL:
//    case Message::Ids::GAMEOVER_RESPONSE:
    default:
        break;
    }

    if (msg && !msg->deserialize(buffer)) {
        delete msg;
        msg = nullptr;
    }

    return msg;
}
