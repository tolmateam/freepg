#include <freepg-common/connection.h>
#include <freepg-common/message.h>
#include <QUdpSocket>
#include <QHostAddress>
#include <QNetworkDatagram>
#include <QDateTime>
#include <assert.h>


/*static */bool Connection::_initialized = false;

Connection::Connection(uint16_t port/* = 0*/)
    : _error(Errors::NO_ERROR), _socket(nullptr)
{
    assert(!_initialized);
    _initialized = true;

    _socket = new QUdpSocket;
    if (!_socket->bind(port)) {
        _error = Errors::BIND_ERROR;
    }

    QObject::connect(_socket, SIGNAL(readyRead()), this, SLOT(readData()));
}

/*virtual */Connection::~Connection()
{
    assert(_initialized);
    if (_socket)
        _socket->deleteLater();

    _initialized = false;
}

/*static */void Connection::advanceSequenceNumber(uint16_t &sequence_number)
{
    sequence_number = static_cast<uint16_t>((sequence_number + 1) % 0xFFFFU);
}

bool Connection::sendTo(const Message &message, const SocketAddress &destination)
{
    QByteArray data = message.serialize();
#ifdef _BYTESTREAM_LOGGING
    {
        QDateTime dt = QDateTime::currentDateTime();
        printf("%s  --[%s:%u]-->  %s\n",
               dt.toString("dd.MM.yyyy hh:mm:ss.zzz").toUtf8().data(),
               destination.address().toString().toUtf8().data(),
               destination.port(),
               data.toHex(' ').data());
    }
#endif
    int64_t count = _socket->writeDatagram(
                data,
                QHostAddress(destination.address()),
                destination.port()
                );
    if (count != message.storageSize()) {
        _error = Errors::SEND_DATA_ERROR;
        return false;
    }

    return true;
}

void Connection::readData()
{
    while (_socket->hasPendingDatagrams()) {
        QNetworkDatagram datagram = _socket->receiveDatagram();
        SocketAddress sender(datagram.senderAddress(), static_cast<uint16_t>(datagram.senderPort()));
#ifdef _BYTESTREAM_LOGGING
        {
            QDateTime dt = QDateTime::currentDateTime();
            printf("%s  <-[%s:%u]---  %s\n",
                   dt.toString("dd.MM.yyyy hh:mm:ss.zzz").toUtf8().data(),
                   sender.address().toString().toUtf8().data(),
                   sender.port(),
                   datagram.data().toHex(' ').data());
        }
#endif
        Message *message = Message::createMessageFromBuffer(datagram.data());
        if (!message) {
            _error = Errors::CANNOT_PARSE_INCOMING_MESSAGE;
            continue;
        }
        incomingMessageHandler(sender, message);
    }
}




//Connection::Connection()
//{
////    uint64_t ms = getMillisecondsSinceEpoch();
////    srand((unsigned int)(ms % (1+(uint64_t)RAND_MAX)));
////    setlocale(LC_ALL, "");
//}
