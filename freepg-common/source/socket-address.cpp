#include <freepg-common/socket-address.h>

SocketAddress::SocketAddress(const QHostAddress &address, uint16_t port)
    : _address(address)
    , _port(port)
{
}

bool SocketAddress::operator==(const SocketAddress &s) const
{
    return _address.isEqual(s._address) && _port == s._port;
}
