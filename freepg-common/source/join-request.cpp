#include <freepg-common/join-request.h>
#include <assert.h>


//------------------
// Static functions
//------------------

/*static */uint16_t JoinRequestMessage::calculateLength(const QString &nickname)
{
    return sizeof(uint16_t) + static_cast<uint16_t>(nickname.toUtf8().size());
}


//--------------------------
// Construction/destruction
//--------------------------

JoinRequestMessage::JoinRequestMessage(const QString &nickname)
    : Message(calculateLength(nickname))
    , _nickname(nickname)
{
    assert(nickname.length() >= GameConsts::NICKNAME_MIN_LENGTH);
    assert(nickname.length() <= GameConsts::NICKNAME_MAX_LENGTH);
    assert(nickname.toUtf8().size() <= GameConsts::NICKNAME_MAX_SIZE);
}


//-------------------------------
// Serialization/deserialization
//-------------------------------

uint16_t JoinRequestMessage::storageSize() const
{
    return Message::storageSize() + calculateLength(_nickname);
}

QByteArray JoinRequestMessage::serialize() const
{
    QByteArray buffer = Message::serialize();
    buffer.reserve(static_cast<int>(JoinRequestMessage::storageSize()));
    QByteArray nickname_utf8 = _nickname.toUtf8();
    return buffer.append(Message::storeUint16(static_cast<uint16_t>(nickname_utf8.size())))
            .append(_nickname.toUtf8());
}

bool JoinRequestMessage::deserialize(const QByteArray &buffer)
{
    size_t size = static_cast<size_t>(buffer.size());
    const char *data = buffer.data();

    if (size < Message::storageSize())
        return false;
    if (!Message::deserialize(buffer))
        return false;
    data += Message::storageSize();
    size -= Message::storageSize();

    if (size < sizeof(uint16_t))
        return false;
    uint16_t nick_size = restoreUint16(data);
    data += sizeof(nick_size);
    size -= sizeof(nick_size);
    if (nick_size > GameConsts::NICKNAME_MAX_SIZE)
        return false;

    if (size < nick_size)
        return false;
    _nickname = QString().fromUtf8(data, static_cast<int>(nick_size));
    if (_nickname.length() < GameConsts::NICKNAME_MIN_LENGTH
            || _nickname.length() > GameConsts::NICKNAME_MAX_LENGTH) {
        return false;
    }
    data += nick_size;
    size -= nick_size;

    return checkLength();
}
