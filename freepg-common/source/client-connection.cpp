#include <freepg-common/client-connection.h>
#include <freepg-common/join-confirm.h>
#include <freepg-common/join-reject.h>
#include <freepg-common/join-request.h>
#include <freepg-common/message.h>
#include <QCoreApplication>
#include <QHostInfo>
#include <QTime>
#include <assert.h>


ClientConnection::ClientConnection(const QString &nickname, const QString &server_address)
    : Connection()
    , _nickname(nickname)
    , _password_hash()
    , _server_address(server_address)
    , _server_ip()
    , _send_sequence_number(0)
    , _receive_sequence_number(0)
    , _started(false)
{
    if (_server_address.isEmpty())
        setError(Connection::Errors::ILLEGAL_NETWORK_ADDRESS);
}

bool ClientConnection::start(QCoreApplication *app)
{
    assert(app);

    if (_started) {
        setError(Connection::Errors::ALREADY_INITIALIZED);
        return false;
    }

    if (!isOk()) {
        return false;
    }

    QHostInfo info = QHostInfo::fromName(_server_address);
    if (info.addresses().empty()) {
        setError(Connection::Errors::NETWORK_ADDRESS_NOT_FOUND);
        return false;
    }
    _server_ip = SocketAddress(info.addresses().first(), Connection::Consts::SERVER_PORT);

    bool result = true;

    // Connecting to server via blocking scenario:

    // 1. Send JoinRequestMessage to the server
    {
        JoinRequestMessage join_request(_nickname);
        result = sendToServer(join_request);
    }
    if (!result) {
        return false;
    }

    // 2. Wait for JoinConfirmMessage
    QTime timer;
    timer.start();
    while (isOk() && _password_hash.isEmpty() && timer.elapsed() < GameConsts::RESPONSE_TIMEOUT_MS) {
        app->processEvents();
    }

    if (!isOk()) {
        return false;
    }

//    if (timer.elapsed() >= GameConsts::RESPONSE_TIMEOUT_MS) {
//        setError(Connection::Errors::RESPONSE_TIMEOUT_REACHED);
//        return false;
//    }

    _started = true;
    return true;
}

bool ClientConnection::sendToServer(Message &message)
{
    switch (message.id()) {
    case Message::Ids::JOIN_REQUEST:
    case Message::Ids::STATUS_RESPONSE:
        break;
    default:
        setError(Errors::ILLEGAL_MESSAGE_TYPE_TO_SEND);
        return false;
    }

    message.setSequenceNumber(_send_sequence_number);
    if (!sendTo(message, _server_ip))
        return false;

    Connection::advanceSequenceNumber(_send_sequence_number);
    return true;
}


void ClientConnection::incomingMessageHandler(const SocketAddress &sender, const Message *message)
{
    assert(message);
    if (sender != _server_ip) {
        // Ignore the message from unknown host
        setError(Connection::Errors::ILLEGAL_MESSAGE_SENDER);
        return;
    }

    size_t sequence_number_delta = (_receive_sequence_number - message->sequenceNumber()) & 0xFFFF;
    if (sequence_number_delta > Connection::Consts::MISSED_DATAGRAM_CRITICAL_COUNT) {
        setError(Connection::Errors::MULTIPLE_MESSAGES_MISSED);
        return;
    } else if (sequence_number_delta > 0) {
        setError(Connection::Errors::MISSED_MESSAGES_DETECTED);
        _receive_sequence_number = message->sequenceNumber();
    }
    Connection::advanceSequenceNumber(_receive_sequence_number);

    bool incoming_message_to_be_emitted = true;

    switch (message->id()) {

    case Message::Ids::JOIN_CONFIRM: {
        const JoinConfirmMessage *msg = static_cast<const JoinConfirmMessage *>(message);
        if (_password_hash.isEmpty()) {
            if (msg->passwordHash().size() == GameConsts::PASSWORD_HASH_SIZE) {
                _password_hash = msg->passwordHash();
            } else {
                setError(Connection::Errors::ILLEGAL_PASSWORD_FORMAT);
            }

        } else {
            setError(Connection::Errors::UNEXPECTED_MESSAGE_RECEIVED);
        }
        incoming_message_to_be_emitted = false;
        break;
    }

    case Message::Ids::JOIN_REJECT: {
//        const JoinRejectMessage *msg = static_cast<const JoinRejectMessage *>(message);
        setError(Connection::Errors::CONNECTION_REJECTED_BY_SERVER);
        incoming_message_to_be_emitted = false;
        break;
    }

    case Message::Ids::STATUS_POLL: {
        break;
    }

    default:
        setError(Errors::ILLEGAL_TYPE_OF_RECEIVED_MESSAGE);
        return;
    }

    if (incoming_message_to_be_emitted)
        emit incomingMessage(message);
}
