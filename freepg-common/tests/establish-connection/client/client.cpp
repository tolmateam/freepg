#include "test-client.h"
#include <freepg-common/client-connection.h>
#include <QDateTime>
#include <QCoreApplication>
#include <stdlib.h>    // srand()
#include <assert.h>    // assert()


int main(int argc, char *argv[])
{
    assert(argc == 2);
    // How to run: ./client client-name

    QCoreApplication app(argc, argv);

    srand(static_cast<unsigned>(QDateTime::currentMSecsSinceEpoch()));
    ClientConnection connection(argv[1], "localhost");
    TestClient client(&connection);

    QObject::connect(
                &connection, SIGNAL(incomingMessage(const Message *)),
                &client, SLOT(incomingMessageHandler(const Message *))
    );

    connection.start(qApp);

    return qApp->exec();
}
