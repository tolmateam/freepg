#pragma once

#include <QObject>
#include <stdint.h>


class ClientConnection;
class Message;

class TestClient : public QObject
{
Q_OBJECT

public:
    TestClient(ClientConnection *connection);
    ~TestClient();

public slots:
    void incomingMessageHandler(const Message *message);

protected:
    enum Consts {
        INACTIVITY_TIME = 10000,
    };
    void timerEvent(QTimerEvent *);

private:
    ClientConnection *_connection = nullptr;
    int _timer_id = 0;
    qint64 _timeout = 0;
};
