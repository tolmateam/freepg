#include "test-client.h"
#include <freepg-common/client-connection.h>
#include <freepg-common/status-poll.h>
#include <freepg-common/status-response.h>
#include <QDateTime>
#include <QCoreApplication>
#include <stdio.h>
#include <assert.h>


TestClient::TestClient(ClientConnection *connection)
    : _connection(connection)
{
    assert(connection);
    _timer_id = startTimer(100, Qt::TimerType::PreciseTimer);
    assert(_timer_id != 0);
    _timeout = QDateTime::currentMSecsSinceEpoch() + Consts::INACTIVITY_TIME;
}


TestClient::~TestClient()
{
    if (_timer_id != 0)
        killTimer(_timer_id);
}


void TestClient::incomingMessageHandler(const Message *message)
{
    assert(message);
    static bool ready = false;

    if (message->id() == Message::Ids::STATUS_POLL) {
        const StatusPollMessage *msg = static_cast<const StatusPollMessage *>(message);
        printf("StatusPoll message (stage=%s) received\n", msg->stageString().toUtf8().data());
        _timeout = QDateTime::currentMSecsSinceEpoch() + Consts::INACTIVITY_TIME;

        switch (msg->stage()) {

        case StatusPollMessage::Stages::BEFORE_GAME: {
            StatusResponseMessage status_response(_connection->nickname(), _connection->passwordHash(), ready);
            _connection->sendToServer(status_response);
            ready = (rand() % 2 == 0);
            break;
        }

        case StatusPollMessage::Stages::GAME_OVER: {
            StatusResponseMessage status_response(_connection->nickname(), _connection->passwordHash(), false);
            _connection->sendToServer(status_response);
            qApp->exit(0);
            break;
        }

        default:
            break;
        }

    } else {
        printf("Unexpected message (type=%u) received\n", message->id());
    }

    delete message;
}


void TestClient::timerEvent(QTimerEvent *)
{
    if ((_timeout > 0 && QDateTime::currentMSecsSinceEpoch() >= _timeout)) {
        qApp->exit(0);
    }
}
