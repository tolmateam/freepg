#pragma once

#include <freepg-common/server-connection.h>
#include <QString>
#include <QObject>
#include <map>


class Message;

struct PlayerData
{
    bool requested = false;
    bool responded = false;
    bool ready = false;
};


class TestServer : public QObject
{
Q_OBJECT

public:
    TestServer(ServerConnection *connection);
    ~TestServer() { }

public slots:
    void clientJoinHandler(ClientHandle client_handle);
    void incomingMessageHandler(const ClientHandle &from, const Message *message);

protected:
    enum States {
        IDLE,
        AWAITING_RESPONSE,
        BETWEEN_POLLS,
        MAX_STATES
    };

    void timerEvent(QTimerEvent *);
    bool handleStateMachine();
    bool isTimeExpired() const;
    void checkIfAllPlayersResponded(bool &all_responded, bool &all_ready) const;
    void handleTimeouts();
    void sendStatusPoll();

private:
    int _timer_id = 0;
    ServerConnection *_connection;
    std::map<QString, PlayerData> _players;
    typedef std::pair<QString, PlayerData> PlayerItem;
    States _state = States::IDLE;
    qint64 _timeout = 0;
    bool _before_game = true;
};
