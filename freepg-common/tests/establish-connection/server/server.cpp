#include "test-server.h"
#include <freepg-common/server-connection.h>
#include <QCoreApplication>
#include <QDateTime>
#include <stdlib.h>    // srand()
#include <assert.h>    // assert()


int main(int argc, char *argv[])
{
    assert(argc == 1);
    // How to run: ./server

    QCoreApplication app(argc, argv);

    srand(static_cast<unsigned>(QDateTime::currentMSecsSinceEpoch()));
    ServerConnection connection;
    TestServer server(&connection);

    QObject::connect(
                &connection, SIGNAL(clientJoinRequest(ClientHandle)),
                &server, SLOT(clientJoinHandler(ClientHandle))
    );
    QObject::connect(
                &connection, SIGNAL(incomingMessage(const ClientHandle &, const Message *)),
                &server, SLOT(incomingMessageHandler(const ClientHandle &, const Message *))
    );

    return qApp->exec();
}
