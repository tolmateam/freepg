#include "test-server.h"
#include <freepg-common/server-connection.h>
#include <freepg-common/status-poll.h>
#include <freepg-common/status-response.h>
#include <QDateTime>
#include <QCoreApplication>
#include <vector>
#include <stdio.h>
#include <assert.h>


TestServer::TestServer(ServerConnection *connection)
    : _connection(connection)
{
    assert(connection);
    _timer_id = startTimer(100, Qt::TimerType::PreciseTimer);
    assert(_timer_id != 0);
}


void TestServer::timerEvent(QTimerEvent *)
{
    if (!handleStateMachine()) {
        killTimer(_timer_id);
        qApp->exit(0);
    }
}


bool TestServer::handleStateMachine()
{
    switch(_state) {

    case States::IDLE:
        if (_connection->clients().size() > 0) {
            _timeout = QDateTime::currentMSecsSinceEpoch() + GameConsts::BETWEEN_STATUS_POLLS_TIMEOUT_MS;
            _before_game = true;
            _state = States::BETWEEN_POLLS;
        }
        break;

    case States::AWAITING_RESPONSE: {
        if (isTimeExpired()) {
            handleTimeouts();
        }
        if (_connection->clients().size() == 0) {
            _timeout = 0;
            _before_game = true;
            _state = States::IDLE;
            break;
        }

        bool all_responded = false;
        bool all_ready = false;
        checkIfAllPlayersResponded(all_responded, all_ready);
        if (all_responded) {

            if (!_before_game) {
                // Just finish the server
                _timeout = 0;
                _state = States::IDLE;
                return false;
            }

            if (all_ready) {
                _before_game = false;
                printf("All clients are ready. Advance the game\n");
            }

            _timeout = QDateTime::currentMSecsSinceEpoch() + GameConsts::BETWEEN_STATUS_POLLS_TIMEOUT_MS;
            _state = States::BETWEEN_POLLS;
        }
        break;
    }

    case States::BETWEEN_POLLS:
        if (isTimeExpired()) {
            sendStatusPoll();
            _timeout = QDateTime::currentMSecsSinceEpoch() + GameConsts::RESPONSE_TIMEOUT_MS;
            _state = States::AWAITING_RESPONSE;
        }
        break;

    default:
        assert("Unknown state" != nullptr);
        break;
    }

    return true;
}


void TestServer::clientJoinHandler(ClientHandle client_handle)
{
    if (_connection->clients().size() < GameConsts::MAX_PLAYERS) {
        _connection->acceptClient(client_handle);

        bool inserted = false;
        for (const auto &c : _connection->clients()) {
            if (c.nickname() == client_handle.nickname()) {
                _players.insert(PlayerItem(client_handle.nickname(), PlayerData()));
                inserted = true;
                break;
            }
        }
        assert(inserted);

    } else {
        _connection->declineClient(client_handle);
    }
}

void TestServer::incomingMessageHandler(const ClientHandle &from, const Message *message)
{
    assert(message);

    if (message->id() == Message::Ids::STATUS_RESPONSE) {
        const StatusResponseMessage *msg = static_cast<const StatusResponseMessage *>(message);

        if (msg->isReady()) {
            printf("StatusResponse message (from %s) received: === R E A D Y ===\n",
                   from.nickname().toUtf8().data());
        } else {
            printf("StatusResponse message (from %s) received: not ready\n",
                   from.nickname().toUtf8().data());
        }

        assert(_players.find(from.nickname()) != _players.end());
        PlayerData &p = _players.at(from.nickname());
        p.responded = true;
        p.ready = msg->isReady();

    } else {
        printf("Unexpected message (from %s, type=%u) received\n",
               from.nickname().toUtf8().data(), message->id());
    }

    delete message;
}

bool TestServer::isTimeExpired() const
{
    return (_timeout > 0 && QDateTime::currentMSecsSinceEpoch() >= _timeout);
}

void TestServer::checkIfAllPlayersResponded(bool &all_responded, bool &all_ready) const
{
    all_responded = true;
    all_ready = true;
    size_t requested_count = 0;

    for (const auto &c : _connection->clients()) {
        assert(_players.find(c.nickname()) != _players.end());
        const PlayerData &p = _players.at(c.nickname());
        if (p.requested) {
            requested_count++;
            all_responded = all_responded && p.responded;
            all_ready = all_ready && p.ready;
        }
    }

    if (requested_count == 0)
        all_ready = false;
}

void TestServer::handleTimeouts()
{
    std::vector<QString> to_delete;

    for (const auto &c : _connection->clients()) {
        assert(_players.find(c.nickname()) != _players.end());
        const PlayerData &p = _players.at(c.nickname());
        if (p.requested && !p.responded) {
            to_delete.push_back(c.nickname());
            printf("Client '%s' has not responded in time\n", c.nickname().toUtf8().data());
        }
    }

    for(auto c : to_delete) {
        _players.erase(c);
        _connection->removeClient(c);
    }
}

void TestServer::sendStatusPoll()
{
    StatusPollMessage::Stages stage = (_before_game)
            ? StatusPollMessage::Stages::BEFORE_GAME
            : StatusPollMessage::Stages::GAME_OVER;

    StatusPollMessage msg(stage);
    printf("\n\n");

    for (const auto &c : _connection->clients()) {
        assert(_players.find(c.nickname()) != _players.end());
        PlayerData &p = _players.at(c.nickname());
        p.requested = true;
        p.responded = false;
        _connection->sendToClient(c, msg);
    }
}
