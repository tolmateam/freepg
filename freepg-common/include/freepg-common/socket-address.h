#pragma once

#include <QHostAddress>
#include <stdint.h>

class SocketAddress
{
public:
    SocketAddress() { }
    SocketAddress(const QHostAddress &address, uint16_t port);

    QHostAddress address() const { return _address; }
    uint16_t port() const { return _port; }

    bool operator==(const SocketAddress &s) const;
    bool operator!=(const SocketAddress &s) const { return !(*this == s); }

private:
    QHostAddress _address;
    uint16_t _port = 0;
};

