#pragma once

#include "message.h"
#include "types.h"


/*
 * StatusPoll message temporary format:
 *     - header (length [16], sequenceNumber [16], id [8])
 *     - stage [8]
 */


class StatusPollMessage : public Message
{
    friend class Message;
protected:
    // To deserialize a message from the buffer
    StatusPollMessage() : Message() { }

public:
    enum Stages {
        MIN_STAGE = 0,
        BEFORE_GAME = 0,
        STAGE_ONE,
        STAGE_TWO,
        STAGE_THREE,
        GAME_OVER,
        MAX_STAGE
    };

    // Construction/destruction
    StatusPollMessage(const Stages &stage);
    ~StatusPollMessage() { }

    uint8_t id() const { return Message::Ids::STATUS_POLL; }

    // Serialization/deserialization
    uint16_t storageSize() const;
    QByteArray serialize() const;
    bool deserialize(const QByteArray &buffer);

    // Members access functions
    Stages stage() const { return _stage; }
    QString stageString() const;

protected:
    static uint16_t calculateLength();

private:
    Stages _stage;
};
