#pragma once

#include "message.h"
#include "types.h"


/*
 * JoinRequest message format:
 *     - header (length [16], sequenceNumber [16], id [8])
 *     - nickname (length [16], [UTF-8])
 */


class JoinRequestMessage : public Message
{
    friend class Message;
protected:
    // To deserialize a message from the buffer
    JoinRequestMessage() : Message() { }

public:
    // Construction/destruction
    JoinRequestMessage(const QString &nickname);
    ~JoinRequestMessage() { }

    uint8_t id() const { return Message::Ids::JOIN_REQUEST; }

    // Serialization/deserialization
    uint16_t storageSize() const;
    QByteArray serialize() const;
    bool deserialize(const QByteArray &buffer);

    // Members access functions
    QString nickname() const { return _nickname; }

protected:
    static uint16_t calculateLength(const QString &nickname);

private:
    QString _nickname;
};
