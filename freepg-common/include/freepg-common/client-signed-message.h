#pragma once

#include "message.h"
#include "types.h"


class ClientSignedMessage : public Message
{
protected:
    // To deserialize a message from the buffer
    ClientSignedMessage() : Message() { }

public:
    // Construction/destruction
    ClientSignedMessage(uint16_t length, const QString &nickname, const QByteArray &password_hash);
    ~ClientSignedMessage() { }

    // Serialization/deserialization
    uint16_t storageSize() const;
    QByteArray serialize() const;
    bool deserialize(const QByteArray &buffer);

    // Members access functions
    QString nickname() const { return _nickname; }
    QByteArray passwordHash() const { return _password_hash; }

protected:
    static uint16_t calculateLength(const QString &nickname, const QByteArray &password_hash);

private:
    QString _nickname;
    QByteArray _password_hash;
};
