#pragma once

#include "connection.h"
//#include <QHostInfo>
#include <QString>
#include <QByteArray>

class Message;
class JoinRequestMessage;


class ClientHandle
{
friend class ServerConnection;

public:
    ClientHandle(const QString &nickname, const SocketAddress &ip);
    bool isOk() const { return _error == Connection::Errors::NO_ERROR; }
    Connection::Errors error() const { return _error; }
    void resetError() { _error = Connection::Errors::NO_ERROR; }
    QString nickname() const { return _nickname; }

protected:
    void setError(Connection::Errors error) { _error = error; }
    void setPasswordHash();

private:
    QString _nickname;
    QByteArray _password_hash;
    SocketAddress _ip;
    uint16_t _send_sequence_number = 0;
    uint16_t _receive_sequence_number = 0;
    enum Connection::Errors _error;
};


class ServerConnection : public Connection
{
Q_OBJECT

public:
    typedef std::vector<ClientHandle> ClientHandles;
    typedef ClientHandles::iterator ClientHandleIterator;
    typedef ClientHandles::const_iterator ConstClientHandleIterator;

public:
    ServerConnection();
    ~ServerConnection() { }

protected:
    ssize_t clientIndexByNickname(const QString &nickname);
    void incomingMessageHandler(const SocketAddress &sender, const Message *message);
    void incomingJoinRequestHandler(const SocketAddress &sender, const JoinRequestMessage *message);

public:
    const ClientHandles &clients() const { return _client_handles; }
    void acceptClient(const ClientHandle &client_handle);
    void declineClient(const ClientHandle &client_handle);
    void removeClient(const QString &nickname);
    bool sendToClient(const ClientHandle &client_handle, Message &message);

signals:
    void clientJoinRequest(ClientHandle client_handle);
    void incomingMessage(const ClientHandle &from, const Message *message);

private:
    ClientHandles _client_handles;
};
