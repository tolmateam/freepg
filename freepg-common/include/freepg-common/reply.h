#pragma once

#include "types.h"


typedef enum reply_consts_t
{
    REPLY_NONE = 0,
    REPLY_START_ZONE,
    REPLY_BUY_STATION,
    REPLY_STAKE,
    REPLY_DROP_STATION,
    REPLY_BUY_FUEL,
    REPLY_BUILDING,
    REPLY_POWER_SUPPLY,
} ReplyTypes;

typedef enum reply_helper_consts
{
    FUEL_LIMIT_PER_MOVE = 18,
    BUILDING_LIMIT_PER_MOVE = 25,
} ReplyHelperConsts;

typedef struct fuel_unit_t
{
    FuelTypes type;
    int price;
} FuelUnit;

typedef struct building_unit_t
{
    int cityId;
    int buildingPrice;
    int linkingPrice;
} BuildingUnit;


ReplyTypes getCurrentReplyType();
int getReplyTimeLeft();
bool setReplyData(ReplyTypes replyType, void *replyData, size_t replyDataLength);

// Struct to be filled with reply data
typedef struct start_zone_reply_t
{
    Zones start_zone;
} StartZoneReply;

typedef struct buy_station_reply_t
{
    int stationId;
    int startPrice;
} BuyStationReply;

typedef struct stake_reply_t
{
    int newPrice;
} StakeReply;

typedef struct drop_station_reply_t
{
    int stationId;
} DropStation;

typedef struct buy_fuel_reply_t
{
    FuelUnit fuelUnits[FUEL_LIMIT_PER_MOVE];
} BuyFuelReply;

typedef struct building_reply_t
{
    BuildingUnit buildingUnits[BUILDING_LIMIT_PER_MOVE];
} BuildingReply;

typedef struct power_supply_reply_t
{
    int suppliedCitiesAmount;
} PowerSupplyReply;
