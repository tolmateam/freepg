#pragma once

#include "socket-address.h"
#include <QUdpSocket>
#include <QString>
#include <QByteArray>
#include <QObject>
#include <stdint.h>

class Message;

class Connection : public QObject
{
Q_OBJECT

    Connection(const Connection &) = delete;
    Connection &operator=(const Connection &) = delete;

public:
    enum Errors {
        NO_ERROR = 0,
        ALREADY_INITIALIZED,
        ILLEGAL_NETWORK_ADDRESS,
        NETWORK_ADDRESS_NOT_FOUND,
        BIND_ERROR,
        ILLEGAL_MESSAGE_TYPE_TO_SEND,
        ILLEGAL_MESSAGE_SENDER,
        MULTIPLE_MESSAGES_MISSED,
        MISSED_MESSAGES_DETECTED,
        CANNOT_PARSE_INCOMING_MESSAGE,
        ILLEGAL_TYPE_OF_RECEIVED_MESSAGE,
        UNEXPECTED_MESSAGE_RECEIVED,
        JOIN_REQUEST_DUPLICATED,
        SEND_DATA_ERROR,
        ILLEGAL_NICKNAME_FORMAT,
        WRONG_PASSWORD,
        ILLEGAL_PASSWORD_FORMAT,
        CONNECTION_REJECTED_BY_SERVER,
        RESPONSE_TIMEOUT_REACHED,
    };

    bool isOk() const { return _error == Errors::NO_ERROR; }
    Errors error() const { return _error; }
    void resetError() { _error = Errors::NO_ERROR; }
    static void advanceSequenceNumber(uint16_t &sequence_number);

protected:
    enum Consts {
        SERVER_PORT = 55789,
        MAX_UDP_DATAGRAM_LENGTH = 508,
        MISSED_DATAGRAM_CRITICAL_COUNT = 10,
    };

    Connection(uint16_t port = 0);
    virtual ~Connection();
    void setError(Errors error) { _error = error; }
    bool sendTo(const Message &message, const SocketAddress &destination);
    virtual void incomingMessageHandler(const SocketAddress &sender, const Message *message) = 0;

protected slots:
    void readData();

private:
    static bool _initialized;
    enum Errors _error;
    QUdpSocket *_socket;
};
