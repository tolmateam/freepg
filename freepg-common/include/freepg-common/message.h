#pragma once

#include "types.h"
#include <QByteArray>
#include <QString>
#include <stdint.h>


class Message
{
public:
    enum Ids {
        JOIN_REQUEST = 61,
        JOIN_CONFIRM = 62,
        JOIN_REJECT = 63,
        STATUS_POLL = 71,
        STATUS_RESPONSE = 72,
//        MOVE_POLL = 73,
//        MOVE_RESPONSE = 74,
//        GAMEOVER_POLL = 75,
//        GAMEOVER_RESPONSE = 76,
        UNKNOWN = 0xFF,
    };

    // Construction/destruction
    static Message *createMessageFromBuffer(const QByteArray &buffer);
protected:
    Message(uint16_t length);
    Message();     // The message the constructor creates is to be deserialized from a buffer later
    Message(const Message &) = delete;
    Message &operator=(const Message &) = delete;
public:
    virtual ~Message();

public:
    // Members access functions
    uint16_t length() const { return _length; }
    uint16_t sequenceNumber() const { return _sequenceNumber; }
    void setSequenceNumber(uint16_t sequence_number) { _sequenceNumber = sequence_number; }
    virtual uint8_t id() const = 0;

protected:
    void increaseLengthBy(uint16_t length) { _length += length; }

public:
    // Serialization/deserialization
    static uint16_t minStorageSize();
    static Message::Ids readIdFromBuffer(const QByteArray &buffer);
    virtual uint16_t storageSize() const;
    virtual QByteArray serialize() const;
    virtual bool deserialize(const QByteArray &buffer);
protected:
    bool checkLength() const { return _length == storageSize() - sizeof(_length); }

protected:
    // Serialization of the simple data to the buffer
    static QByteArray storeUint8(uint8_t value);
    static QByteArray storeUint16(uint16_t value);
    static QByteArray storeUint32(uint32_t value);
    static QByteArray storeInt8(int8_t value);
    static QByteArray storeInt16(int16_t value);
    static QByteArray storeInt32(int32_t value);

    // Deserialization of the simple data from the buffer
    static uint8_t restoreUint8(const char *buffer);
    static uint16_t restoreUint16(const char *buffer);
    static uint32_t restoreUint32(const char *buffer);
    static int8_t restoreInt8(const char *buffer);
    static int16_t restoreInt16(const char *buffer);
    static int32_t restoreInt32(const char *buffer);

private:
    // The 'length' field value does not include the length of 'length' field itself.
    uint16_t _length;
    uint16_t _sequenceNumber;
    uint32_t _padding = 0; // To make the syntax analyzer silent
};
