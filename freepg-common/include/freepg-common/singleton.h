//
// The signleton template implementation is from
// https://codereview.stackexchange.com/questions/173929/modern-c-singleton-template
//

template <typename T>
class Singleton
{
public:
    static T &instance()
    {
        static MemGuard g; // To clean up on program end
        if (!_instance)
            _instance = new T;
        return *_instance;
    }
    Singleton(const Singleton &) = delete;
    Singleton &operator=(const Singleton &) = delete;

protected:
    Singleton() { }
    virtual ~Singleton() { }

private:
    inline static T *_instance = nullptr;

    class MemGuard
    {
    public:
        ~MemGuard()
        {
            if (_instance)
                delete _instance;
            _instance = nullptr;
        }
    };
};
