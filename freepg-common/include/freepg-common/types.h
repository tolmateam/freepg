#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>


//#define _LOGGING
//#define _LOGGING_BINARY_DATA

//enum GameStages
//{
//    BEFORE_GAME_STAGE = 0,
//    GAME_FIRST_STAGE,
//    GAME_SECOND_STAGE,
//    GAME_THIRD_STAGE,
//    GAME_OVER_STAGE
//};

//enum GamePhases
//{
//    NONE_PHASE = 0,
//    AUCTION_PHASE,
//    RESOURCE_PHASE,
//    BUILDING_PHASE,
//    SUPPLY_PHASE,
//};

//enum PlayerActions
//{
//    READY_ACTION = 0,
//    START_TRADE_ACTION,
//    BID_ACTION,
//    DROP_EXTRA_STATION_ACTION,
//    BUY_RESOURCE_ACTION,
//    LINK_CITY_ACTION,
//    SUPPLY_ACTION,
//};

enum GameConsts
{
//    MAX_CITY_NAME_LENGTH = 64,
//    MIN_PLAYERS = 2,
    MAX_PLAYERS = 6,
    NICKNAME_MIN_LENGTH = 3,
    NICKNAME_MAX_LENGTH = 32,
    NICKNAME_MAX_SIZE = 64,
    PASSWORD_HASH_SIZE = 16,
    RESPONSE_TIMEOUT_MS = 2000,
    BETWEEN_STATUS_POLLS_TIMEOUT_MS = 1000,
//    MAX_STATIONS_ON_HANDS = 3,
//    CITIES_PER_PLAYER = 7,
//    FIRST_CITY_SUPPLIER = 10,
//    SECOND_CITY_SUPPLIER = 15,
//    THIRD_CITY_SUPPLIER = 20,
//    MAX_ORDER_BOOK_DEPTH = 16,
//    MAX_ACTION_ITEMS = CITIES_PER_PLAYER * MAX_PLAYERS,
};

//enum Zones
//{
//    RED_ZONE = 0,
//    GREEN_ZONE,
//    BLUE_ZONE,
//    BROWN_ZONE,
//    YELLOW_ZONE,
//    MAGENTA_ZONE,
//    MAX_ZONE
//};

//enum FuelTypes
//{
//    FT_COAL = 0,
//    FT_HEATING_OIL,
//    FT_WASTE,
//    FT_URANIUM,
//    FT_MAX,
//};

//struct City
//{
//    const char *name;
//    int id;
//    int zone;
//};

//struct PowerLine
//{
//    int city1;
//    int price;
//    int city2;
//};

//struct PowerStation
//{
//    uint8_t price;
//    uint8_t suppliedCitiesAmount;
//    uint8_t fuelAmount[FT_MAX];
//};

//struct Player
//{
//    char nickname[NICKNAME_MAX_LENGTH+1];
//    bool ready;
//};

//struct GameState
//{
//    uint16_t protocolVersion;
//    size_t playersAmount;
//    size_t myPlayer;
//    Player players[MAX_PLAYERS];
////    size_t citiesAmount;
////    const City *cities;
////    size_t linksAmount;
////    const PowerLine *powerLines;
////    size_t stationsAmount;
////    const PowerStation *stations;
//    bool connected;
//    bool gameStarted;
//};


//uint64_t getWideRandomValue();
//void milliSleep(unsigned long ms);
//uint64_t getMillisecondsSinceEpoch();
//void protocolVersionToString(uint16_t protocolVersion, char *str);

#ifdef _LOGGING
    void logPrintf(const char * format, ...);
#else
    static inline void logPrintf(const char *format, ...) { (void)format; }
#endif // _LOGGING
