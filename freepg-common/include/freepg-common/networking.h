#pragma once

#include "message.h"
#include "status-poll.h"
#include "types.h"
#include <wchar.h>


enum NetworkProtocolConsts
{
    REGULAR_SEND_TIMEOUT = 500,
    REGULAR_RECEIVE_TIMEOUT = 5000,
    SHORT_RECEIVE_TIMEOUT = 100,
    SURVEY_DEADLINE = 2500,
    MAX_PROTOCOL_ERRORS_ALLOWED = 2,
    CONNECTION_TIMEOUT = 3500,
};

typedef enum error_causes_t
{
    OK_CAUSE = 0,
    INVALID_ARGUMENT_CAUSE = 1,
    INTERNAL_CAUSE = 2,
    CONNECTION_CAUSE = 3,
    PROTOCOL_VERSION_CAUSE = 4,
} ErrorCauses;

typedef enum server_reject_causes_t
{
    OK_REJECT_CAUSE = 0,
    // Join reject causes
    INVALID_NICKNAME_CAUSE = 1,
    EXTRA_PLAYER_CAUSE = 2,

    // Knock out causes
    MOVE_TIMEOUT = 3,
} ServerRejectCauses;


typedef struct owned_station_t
{
    int index;
    uint8_t fuelReserve[FT_MAX];
} OwnedStation;

typedef struct player_data_t
{
    wchar_t nickname[NICKNAME_MAX_LENGTH+1];
    uint8_t password[PASSWORD_HASH_LENGTH];
    bool respond;
    bool ready;
    int networkErrorCounter;
    Zones zone;
    uint16_t money;
    size_t stationsAmount;
    OwnedStation stations[MAX_STATIONS_ON_HANDS + 1];
    size_t citiesAmount;
    size_t cities[CITIES_PER_PLAYER * MAX_PLAYERS]; // Each array item contains city index
    void *aiHandle;
} PlayerData;

typedef struct trader_t
{
    bool allowed; // The trader is still allowed to participate in the remaining auctions.
    bool active;  // The trader is still active in the auction currently is in progress.
} Trader;

typedef struct auction_t
{
    int stationIndex;
    uint16_t currentBid;
    int bidder;
    int starter;
    int currentPlayer;
    Trader traders[MAX_PLAYERS];
    bool inProgress;
    bool anythingSold;
} Auction;

typedef struct order_book_t
{
    // Each cell contains the price or zero if there no futher goods.
    // The higher depth index the higher price.
    size_t depth;
    uint8_t price[MAX_ORDER_BOOK_DEPTH];
} OrderBook;

typedef struct resource_market_t
{
    // The order book for each fuel type
    OrderBook orderBook[FT_MAX];
} ResourceMarket;

typedef struct server_handle_t
{
    bool initialized;
    int socket;
    int endpoint;
    uint16_t sequenceNumber;
    size_t playersAmount;
    PlayerData players[MAX_PLAYERS];
    size_t playersToDisconnectAmount;
    DisconnectedPlayer playersToDisconnect[MAX_PLAYERS_TO_DISCONNECT];
    GameStages gameStage;
    GamePhases gamePhase;
    Auction auction;
    ResourceMarket resourceMarket;
    size_t playerToMove;
} ServerHandle;

typedef struct client_handle_t
{
    bool initialized;
    int socket;
    int connection;

    wchar_t nickname[NICKNAME_MAX_LENGTH+1];
    uint8_t password[PASSWORD_HASH_LENGTH];
    uint16_t sequenceNumber;

    size_t actionItemsAmount;
    PlayerActions action;
    uint16_t actionItemsContent[MAX_ACTION_ITEMS];

    GameState gameState;
} ClientHandle;


// Useful functions


// Common functions

uint16_t getLocalProtocolVersion();


// Client functions

ErrorCauses startClient(ClientHandle *handle, const char *serverAddress, const wchar_t *nickname, const wchar_t *password);
void stopClient(ClientHandle *handle);
ErrorCauses respondToServer(ClientHandle *handle);

const GameState *getGameState(const ClientHandle *handle);
void setClientReady(ClientHandle *handle, bool ready);


// Server functions

ErrorCauses startServer(ServerHandle *handle);
void stopServer(ServerHandle *handle);
ErrorCauses pollingClients(ServerHandle *handle);
