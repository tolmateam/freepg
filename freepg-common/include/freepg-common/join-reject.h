#pragma once

#include "message.h"
#include "types.h"


/*
 * JoinReject message format:
 *     - header (length [16], sequenceNumber [16], id [8])
 *     - cause [8]
 */


class JoinRejectMessage : public Message
{
    friend class Message;
protected:
    // To deserialize a message from the buffer
    JoinRejectMessage() : Message() { }

public:
    enum Causes {
        MIN_CAUSE = 0,
        INCORRECT_REQUEST = 0,
        NICKNAME_BUSY,
        NO_ROOM_TO_PLAY,
        MAX_CAUSE
    };

    // Construction/destruction
    JoinRejectMessage(const Causes &cause);
    ~JoinRejectMessage() { }

    uint8_t id() const { return Message::Ids::JOIN_REJECT; }

    // Serialization/deserialization
    uint16_t storageSize() const;
    QByteArray serialize() const;
    bool deserialize(const QByteArray &buffer);

    // Members access functions
    Causes cause() const { return _cause; }

protected:
    static uint16_t calculateLength();

private:
    Causes _cause;
};
