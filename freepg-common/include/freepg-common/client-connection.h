#pragma once

#include "connection.h"
#include <QHostInfo>
#include <QString>
#include <QByteArray>

class Message;
class SocketAddress;
class QCoreApplication;

class ClientConnection : public Connection
{
Q_OBJECT

public:
    // Initialization/deinitialization
    ClientConnection(const QString &nickname, const QString &server_address);
    ~ClientConnection() { }
    bool start(QCoreApplication *app);

    // Member access functions
    QString nickname() const { return _nickname; }
    QByteArray passwordHash() const { return _password_hash; }
    QString serverAddress() const { return _server_address; }

    // Control functions
    bool sendToServer(Message &message);
protected:
    void incomingMessageHandler(const SocketAddress &sender, const Message *message);

signals:
    void incomingMessage(const Message *message);

private:
    QString _nickname;
    QByteArray _password_hash;
    QString _server_address;
    SocketAddress _server_ip;
    uint16_t _send_sequence_number;
    uint16_t _receive_sequence_number;
    bool _started;
};
