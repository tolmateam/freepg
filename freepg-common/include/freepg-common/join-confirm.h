#pragma once

#include "message.h"
#include "types.h"


/*
 * JoinConfirm message format:
 *     - header (length [16], sequenceNumber [16], id [8])
 *     - password-hash [8 * PASSWORD_HASH_SIZE]
 */


class JoinConfirmMessage : public Message
{
    friend class Message;
protected:
    // To deserialize a message from the buffer
    JoinConfirmMessage() : Message() { }

public:
    // Construction/destruction
    JoinConfirmMessage(const QByteArray &password_hash);
    ~JoinConfirmMessage() { }

    uint8_t id() const { return Message::Ids::JOIN_CONFIRM; }

    // Serialization/deserialization
    uint16_t storageSize() const;
    QByteArray serialize() const;
    bool deserialize(const QByteArray &buffer);

    // Members access functions
    QByteArray passwordHash() const { return _password_hash; }

protected:
    static uint16_t calculateLength();

private:
    QByteArray _password_hash;
};
