#pragma once

#include "client-signed-message.h"
#include "types.h"


/*
 * StatusResponse message temporary format:
 *     - header (length [16], sequenceNumber [16], id [8])
 *     - player credentials (nickname (length [16], [UTF-8]), password-hash [8 * PASSWORD_HASH_SIZE])
 *     - ready [8]
 */


class StatusResponseMessage : public ClientSignedMessage
{
    friend class Message;
protected:
    // To deserialize a message from the buffer
    StatusResponseMessage() : ClientSignedMessage() { }

public:
    // Construction/destruction
    StatusResponseMessage(const QString &nickname, const QByteArray &password_hash, bool ready);
    ~StatusResponseMessage() { }

    uint8_t id() const { return Message::Ids::STATUS_RESPONSE; }

    // Serialization/deserialization
    uint16_t storageSize() const;
    QByteArray serialize() const;
    bool deserialize(const QByteArray &buffer);

    // Members access functions
    bool isReady() const { return _ready; }

protected:
    static uint16_t calculateLength();

private:
    bool _ready;
};
